using NUnit.Framework;
using System;
using System.Net;
using Tiingo.WebCode;

namespace Tiingo.WebCodeTest
{
    public class WebCodeTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test, Description("Test Web Status")]
        public void TestWebStatus()
        {
            WebStatus status = new WebStatus((int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetTypeCode()),
                                             "Success.");
            Assert.AreEqual(status.Code, 200);
            Assert.AreEqual(status.Message, "Success.");

            status = new WebStatus(HttpStatusCode.OK, "Success.");
            Assert.AreEqual(status.Code, 200);
            Assert.AreEqual(status.Message, "Success.");

            Exception ex = Assert.Throws<ArgumentNullException>(delegate { new WebStatus(200, null); });
            Assert.IsInstanceOf(typeof(ArgumentNullException), ex);
        }
    }
}