﻿using System;
using System.Net;

namespace Tiingo.WebCode
{
    public class WebStatus
    {
        public const string MSG_HTTPOK = "Success.";

        private int code;
        private string message;

        public WebStatus(int code,
                         string message)
        {
            Code = code;
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }

        public WebStatus(HttpStatusCode code,
                         string message)
        {
            Code = (int)Convert.ChangeType(code, code.GetTypeCode());
            Message = message ?? throw new ArgumentNullException(nameof(message));
        }

        public WebStatus()
        {

        }

        public string Message { get => message; set => message = value; }
        public int Code { get => code; set => code = value; }
    }
}
