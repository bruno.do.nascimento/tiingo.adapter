﻿using Newtonsoft.Json.Linq;
using System;

namespace Tiingo.Metadata
{
    public class StockMetadata
    {
        private string name;
        private DateTime? startDate;
        private string ticker;
        private string? exchangeCode;
        private string? description;
        private DateTime? endDate;

        public StockMetadata()
        {
        }

        public StockMetadata(string name,
                             DateTime? startDate,
                             string ticker,
                             string? exchangeCode,
                             string? description,
                             DateTime? endDate)
        {
            Name = name;
            StartDate = startDate;
            Ticker = ticker;
            ExchangeCode = exchangeCode;
            Description = description;
            EndDate = endDate;
        }

        public StockMetadata(JToken token)
        {
            Name = (string)token.SelectToken("name");
            StartDate = (DateTime?)token.SelectToken("startDate");
            Ticker = (string)token.SelectToken("ticker");
            ExchangeCode = (string?)token.SelectToken("exchangeCode");
            Description = (string?)token.SelectToken("description");
            EndDate = (DateTime?)token.SelectToken("endDate");
        }

        public string Name { get => name; set => name = value; }
        public DateTime? StartDate { get => startDate; set => startDate = value; }
        public string Ticker { get => ticker; set => ticker = value; }
        public string? ExchangeCode { get => exchangeCode; set => exchangeCode = value; }
        public string? Description { get => description; set => description = value; }
        public DateTime? EndDate { get => endDate; set => endDate = value; }
    }
}
