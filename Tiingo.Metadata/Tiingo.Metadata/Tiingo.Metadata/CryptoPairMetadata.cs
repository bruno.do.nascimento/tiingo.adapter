﻿using Newtonsoft.Json.Linq;

namespace Tiingo.Metadata
{
    public class CryptoPairMetadata
    {
        private string? ticker;
        private string? name;
        private string? description;
        private string? baseCurrency;
        private string? quoteCurrency;

        public CryptoPairMetadata()
        {

        }

        public CryptoPairMetadata(string? ticker,
                                  string? name,
                                  string? description,
                                  string? baseCurrency,
                                  string? quoteCurrency)
        {
            Ticker = ticker;
            Name = name;
            Description = description;
            BaseCurrency = baseCurrency;
            QuoteCurrency = quoteCurrency;
        }

        public CryptoPairMetadata(JToken token)
        {
            Ticker = (string?)token.SelectToken("ticker");
            Name = (string?)token.SelectToken("name");
            Description = (string?)token.SelectToken("description");
            BaseCurrency = (string?)token.SelectToken("baseCurrency");
            QuoteCurrency = (string?)token.SelectToken("quoteCurrency");
        }

        public string? Ticker { get => ticker; set => ticker = value; }
        public string? Name { get => name; set => name = value; }
        public string? Description { get => description; set => description = value; }
        public string? BaseCurrency { get => baseCurrency; set => baseCurrency = value; }
        public string? QuoteCurrency { get => quoteCurrency; set => quoteCurrency = value; }
    }
}
