using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using Tiingo.Metadata;

namespace Tiingo.MetadataTest
{
    public class MetadataTests
    {
        private const string METADATA_STRING = "{\"name\": \"Apple Inc\", \"startDate\": \"1980-12-12\", \"ticker\": \"AAPL\", \"exchangeCode\": \"NASDAQ\", \"description\": \"Apple Inc. (Apple) designs, manufactures and markets mobile communication and media devices, personal computers, and portable digital music players, and a variety of related software, services, peripherals, networking solutions, and third-party digital content and applications. The Company's products and services include iPhone, iPad, Mac, iPod, Apple TV, a portfolio of consumer and professional software applications, the iOS and OS X operating systems, iCloud, and a variety of accessory, service and support offerings. The Company also delivers digital content and applications through the iTunes Store, App StoreSM, iBookstoreSM, and Mac App Store. The Company distributes its products worldwide through its retail stores, online stores, and direct sales force, as well as through third-party cellular network carriers, wholesalers, retailers, and value-added resellers. In February 2012, the Company acquired app-search engine Chomp.\", \"endDate\": \"2020-05-05\"}";
        private const string CRYPTO_METADATA = "{\"name\":\"Bitcoin Tether (BTC/USD)\",\"quoteCurrency\":\"usd\",\"description\":\"Tether (BTC/USD)\",\"ticker\":\"btcusd\",\"baseCurrency\":\"btc\"}";
        [SetUp]
        public void Setup()
        {
        }

        [Test, Description("Test class StockMetadata")]
        public void TestTiingoMetadata()
        {
            StockMetadata stockData = new StockMetadata(JToken.Parse(METADATA_STRING));
            Assert.AreEqual(stockData.Name, "Apple Inc");
            Assert.AreEqual(stockData.StartDate, new DateTime(1980, 12, 12));
            Assert.AreEqual(stockData.Ticker, "AAPL");
            Assert.AreEqual(stockData.ExchangeCode, "NASDAQ");
            Assert.AreEqual(stockData.Description, "Apple Inc. (Apple) designs, manufactures and markets mobile communication and media devices, personal computers, and portable digital music players, and a variety of related software, services, peripherals, networking solutions, and third-party digital content and applications. The Company's products and services include iPhone, iPad, Mac, iPod, Apple TV, a portfolio of consumer and professional software applications, the iOS and OS X operating systems, iCloud, and a variety of accessory, service and support offerings. The Company also delivers digital content and applications through the iTunes Store, App StoreSM, iBookstoreSM, and Mac App Store. The Company distributes its products worldwide through its retail stores, online stores, and direct sales force, as well as through third-party cellular network carriers, wholesalers, retailers, and value-added resellers. In February 2012, the Company acquired app-search engine Chomp.");
            Assert.AreEqual(stockData.EndDate, new DateTime(2020, 05, 05));
        }

        [Test, Description("Test class CryptoMetadata")]
        public void TestTiingoCryptoMetadata()
        {
            CryptoPairMetadata cryptoData = new CryptoPairMetadata(JToken.Parse(CRYPTO_METADATA));
            Assert.AreEqual(cryptoData.Ticker, "btcusd");
            Assert.AreEqual(cryptoData.Name, "Bitcoin Tether (BTC/USD)");
            Assert.AreEqual(cryptoData.Description, "Tether (BTC/USD)");
            Assert.AreEqual(cryptoData.BaseCurrency, "btc");
            Assert.AreEqual(cryptoData.QuoteCurrency, "usd");
        }
    }
}