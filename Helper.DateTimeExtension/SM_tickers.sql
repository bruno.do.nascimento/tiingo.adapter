USE [GlobalMarket]
GO

/****** Object:  Table [dbo].[SM_tickers]    Script Date: 26/04/2020 18:55:43 ******/
DROP TABLE [dbo].[SM_tickers]
GO

/****** Object:  Table [dbo].[SM_tickers]    Script Date: 26/04/2020 18:55:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SM_tickers](
	[symbol] [varchar](10) NOT NULL,
	[company_name] [ntext] NULL,
	[IPO_year] [int] NULL,
	[sector] [ntext] NULL,
	[industry] [ntext] NULL,
	[summary_quote] [ntext] NULL,
	[stock_exchange] [varchar](10) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


