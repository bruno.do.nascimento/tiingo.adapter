INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '01/01/2015', 103),'New Year''s Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '19/01/2015', 103),'Martin Luther King Jr. Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '16/02/2015', 103),'President''s Day & George Washington''s Birthday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '03/04/2015', 103),'Good Friday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '25/05/2015', 103),'Memorial Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '03/07/2015', 103),'Independence Day Observance');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '07/09/2015', 103),'Labor Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '26/11/2015', 103),'Thanksgiving Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2015, TRY_CONVERT(DATETIME2, '25/12/2015', 103),'Christmas Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '01/01/2016', 103),'New Year''s Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '18/01/2016', 103),'Martin Luther King Jr. Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '15/02/2016', 103),'President''s Day & George Washington''s Birthday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '25/03/2016', 103),'Good Friday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '30/05/2016', 103),'Memorial Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '04/07/2016', 103),'Independence Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '05/09/2016', 103),'Labor Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '24/11/2016', 103),'Thanksgiving Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2016, TRY_CONVERT(DATETIME2, '26/12/2016', 103),'Christmas Day Observance');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '02/01/2017', 103),'New Year''s Day Observance');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '16/01/2017', 103),'Martin Luther King Jr. Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '20/02/2017', 103),'President''s Day & George Washington''s Birthday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '14/04/2017', 103),'Good Friday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '29/05/2017', 103),'Memorial Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '04/07/2017', 103),'Independence Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '04/09/2017', 103),'Labor Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '23/11/2017', 103),'Thanksgiving Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2017, TRY_CONVERT(DATETIME2, '25/12/2017', 103),'Christmas Day Observance');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '01/01/2018', 103),'New Year''s Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '15/01/2018', 103),'Martin Luther King Jr. Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '19/02/2018', 103),'President''s Day & George Washington''s Birthday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '30/03/2018', 103),'Good Friday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '28/05/2018', 103),'Memorial Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '04/07/2018', 103),'Independence Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '03/09/2018', 103),'Labor Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '22/11/2018', 103),'Thanksgiving Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '05/12/2018', 103),'Market''s Holiday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2018, TRY_CONVERT(DATETIME2, '25/12/2018', 103),'Christmas Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '01/01/2019', 103),'New Year''s Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '21/01/2019', 103),'Martin Luther King Jr. Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '18/02/2019', 103),'President''s Day & George Washington''s Birthday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '19/04/2019', 103),'Good Friday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '27/05/2019', 103),'Memorial Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '04/07/2019', 103),'Independence Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '02/09/2019', 103),'Labor Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '28/11/2019', 103),'Thanksgiving Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2019, TRY_CONVERT(DATETIME2, '25/12/2019', 103),'Christmas Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '01/01/2020', 103),'New Year''s Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '20/01/2020', 103),'Martin Luther King Jr. Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '17/02/2020', 103),'President''s Day & George Washington''s Birthday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '10/04/2020', 103),'Good Friday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '25/05/2020', 103),'Memorial Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '03/07/2020', 103),'Independence Day Observance');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '07/09/2020', 103),'Labor Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '26/11/2020', 103),'Thanksgiving Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2020, TRY_CONVERT(DATETIME2, '25/12/2020', 103),'Christmas Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '01/01/2021', 103),'New Year''s Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '18/01/2021', 103),'Martin Luther King Jr. Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '15/02/2021', 103),'President''s Day & George Washington''s Birthday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '02/04/2021', 103),'Good Friday');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '31/05/2021', 103),'Memorial Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '05/07/2021', 103),'Independence Day Observance');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '06/09/2021', 103),'Labor Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '25/11/2021', 103),'Thanksgiving Day');
INSERT INTO [dbo].[SM_holidays] (year, stock_holiday, observance) VALUES (2021, TRY_CONVERT(DATETIME2, '24/12/2021', 103),'Christmas Day Observance');
