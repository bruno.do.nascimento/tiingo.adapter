USE [GlobalMarket]
GO

/****** Object:  Table [dbo].[SM_holidays]    Script Date: 26/04/2020 18:12:59 ******/
DROP TABLE [dbo].[SM_holidays]
GO

/****** Object:  Table [dbo].[SM_holidays]    Script Date: 26/04/2020 18:12:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SM_holidays](
	[year] [int] NOT NULL,
	[stock_holiday] [datetime2](7) NOT NULL,
	[observance] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


