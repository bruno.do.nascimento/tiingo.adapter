USE [GlobalMarket]
GO

/****** Object:  UserDefinedFunction [dbo].[holidays_table]    Script Date: 29/04/2020 19:37:00 ******/
DROP FUNCTION [dbo].[holidays_table]
GO

/****** Object:  UserDefinedFunction [dbo].[holidays_table]    Script Date: 29/04/2020 19:37:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[holidays_table] 
(
	@year int,
	@date_format int
)
RETURNS 
@holidays TABLE 
(
	holiday NVARCHAR(40) NOT NULL,
	observance NTEXT
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	INSERT INTO @holidays
	SELECT CONVERT(NVARCHAR, stock_holiday, @date_format) stock_holiday,
		   observance
	FROM   SM_holidays
	WHERE  year = @year
	RETURN 
END
GO


