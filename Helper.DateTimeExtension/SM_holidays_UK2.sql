USE [GlobalMarket]
GO

/****** Object:  Index [SM_holidays_UK2]    Script Date: 26/04/2020 18:15:28 ******/
DROP INDEX [SM_holidays_UK2] ON [dbo].[SM_holidays]
GO

/****** Object:  Index [SM_holidays_UK2]    Script Date: 26/04/2020 18:15:28 ******/
CREATE UNIQUE NONCLUSTERED INDEX [SM_holidays_UK2] ON [dbo].[SM_holidays]
(
	[year] ASC,
	[stock_holiday] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO


