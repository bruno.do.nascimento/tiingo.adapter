USE [GlobalMarket]
GO

/****** Object:  Index [SM_tickers_IX1]    Script Date: 26/04/2020 19:21:50 ******/
DROP INDEX [SM_tickers_IX1] ON [dbo].[SM_tickers]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [SM_tickers_IX1]    Script Date: 26/04/2020 19:21:50 ******/
CREATE NONCLUSTERED INDEX [SM_tickers_IX1] ON [dbo].[SM_tickers]
(
	[stock_exchange] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO


