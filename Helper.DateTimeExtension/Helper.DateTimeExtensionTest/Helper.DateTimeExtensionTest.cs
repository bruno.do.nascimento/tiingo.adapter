using Helper.DateTimeExtension;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Helper.DateTimeExtensionTest
{
    public class DateTimeExtensionTests
    {
        private IDictionary<int, IDictionary<DateTime, string>> testList;
        private IEnumerable<DateTime> yearHolidays;

        [SetUp]
        public void Setup()
        {
            testList = DateTimeExtensions.LoadHolidays();
            yearHolidays = DateTimeExtensions.GetHolidays(new List<int> { 2019, 2020, 2021 }.AsEnumerable(),
                                                          testList);
        }

        [Test, Description("Load all holidays from database")]
        public void TestLoadHolidays()
        {
            Assert.IsNotNull(testList);
            List<KeyValuePair<int, IDictionary<DateTime, string>>> testHolidaysList = testList.ToList();
            foreach (KeyValuePair<int, IDictionary<DateTime, string>> testYearHolidays in testHolidaysList)
            {
                Console.WriteLine("Year: {0}", testYearHolidays.Key);
            }
            Assert.IsTrue(testList.Count > 0);
        }

        [Test, Description("Get all holidays from three different years")]
        public void TestGetHolidays()
        {
            Assert.IsNotNull(yearHolidays);
            foreach (DateTime holiday in yearHolidays)
            {
                Console.WriteLine("Holiday: {0}", holiday.ToString(ConversionHelper.SHORTDATE_FORMAT));
            }
            Assert.IsTrue(yearHolidays.Count() > 0);
        }

        [Test, Description("Get all holidays from three repeated years")]
        public void TestGetRepeatedYearsHolidays()
        {
            IEnumerable<DateTime> repYearHolidays = DateTimeExtensions.GetHolidays(years: new List<int> { 2020, 2020, 2020 }.AsEnumerable(),
                                                                                testList);
            Assert.IsNotNull(repYearHolidays);
            foreach (DateTime holiday in repYearHolidays)
            {
                Console.WriteLine("Holiday: {0}", holiday.ToString(ConversionHelper.SHORTDATE_FORMAT));
            }
            Assert.IsTrue(repYearHolidays.Count() > 0);
        }

        [Test, Description("Get Easter Sunday for any given year")]
        public void TestGetEasterSunday()
        {
            DateTime easter = DateTimeExtensions.GetEasterSunday(2015);
            Assert.AreEqual(easter, new DateTime(2015, 4, 5));
            easter = DateTimeExtensions.GetEasterSunday(2016);
            Assert.AreEqual(easter, new DateTime(2016, 3, 27));
            easter = DateTimeExtensions.GetEasterSunday(2017);
            Assert.AreEqual(easter, new DateTime(2017, 4, 16));
            easter = DateTimeExtensions.GetEasterSunday(2018);
            Assert.AreEqual(easter, new DateTime(2018, 4, 1));
            easter = DateTimeExtensions.GetEasterSunday(2019);
            Assert.AreEqual(easter, new DateTime(2019, 4, 21));
            easter = DateTimeExtensions.GetEasterSunday(2020);
            Assert.AreEqual(easter, new DateTime(2020, 4, 12));
            easter = DateTimeExtensions.GetEasterSunday(2021);
            Assert.AreEqual(easter, new DateTime(2021, 4, 4));
        }

        [Test, Description("Get Good Friday for any given year")]
        public void TestGetGoodFriday()
        {
            DateTime goodFriday = DateTimeExtensions.GetGoodFriday(2015);
            Assert.AreEqual(goodFriday, new DateTime(2015, 4, 3));
            goodFriday = DateTimeExtensions.GetGoodFriday(2016);
            Assert.AreEqual(goodFriday, new DateTime(2016, 3, 25));
            goodFriday = DateTimeExtensions.GetGoodFriday(2017);
            Assert.AreEqual(goodFriday, new DateTime(2017, 4, 14));
            goodFriday = DateTimeExtensions.GetGoodFriday(2018);
            Assert.AreEqual(goodFriday, new DateTime(2018, 3, 30));
            goodFriday = DateTimeExtensions.GetGoodFriday(2019);
            Assert.AreEqual(goodFriday, new DateTime(2019, 4, 19));
            goodFriday = DateTimeExtensions.GetGoodFriday(2020);
            Assert.AreEqual(goodFriday, new DateTime(2020, 4, 10));
            goodFriday = DateTimeExtensions.GetGoodFriday(2021);
            Assert.AreEqual(goodFriday, new DateTime(2021, 4, 2));
        }

        [Test, Description("Get Easter Monday for any given year")]
        public void TestGetEasterMonday()
        {
            DateTime easterMonday = DateTimeExtensions.GetEasterMonday(2015);
            Assert.AreEqual(easterMonday, new DateTime(2015, 4, 6));
            easterMonday = DateTimeExtensions.GetEasterMonday(2016);
            Assert.AreEqual(easterMonday, new DateTime(2016, 3, 28));
            easterMonday = DateTimeExtensions.GetEasterMonday(2017);
            Assert.AreEqual(easterMonday, new DateTime(2017, 4, 17));
            easterMonday = DateTimeExtensions.GetEasterMonday(2018);
            Assert.AreEqual(easterMonday, new DateTime(2018, 4, 2));
            easterMonday = DateTimeExtensions.GetEasterMonday(2019);
            Assert.AreEqual(easterMonday, new DateTime(2019, 4, 22));
            easterMonday = DateTimeExtensions.GetEasterMonday(2020);
            Assert.AreEqual(easterMonday, new DateTime(2020, 4, 13));
            easterMonday = DateTimeExtensions.GetEasterMonday(2021);
            Assert.AreEqual(easterMonday, new DateTime(2021, 4, 5));
        }

        [Test, Description("Get Pasquetta - Italian name for Easter Monday - for any given year")]
        public void TestGetPasquetta()
        {
            DateTime pasquetta = DateTimeExtensions.GetPasquetta(2015);
            Assert.AreEqual(pasquetta, new DateTime(2015, 4, 6));
            pasquetta = DateTimeExtensions.GetPasquetta(2016);
            Assert.AreEqual(pasquetta, new DateTime(2016, 3, 28));
            pasquetta = DateTimeExtensions.GetPasquetta(2017);
            Assert.AreEqual(pasquetta, new DateTime(2017, 4, 17));
            pasquetta = DateTimeExtensions.GetPasquetta(2018);
            Assert.AreEqual(pasquetta, new DateTime(2018, 4, 2));
            pasquetta = DateTimeExtensions.GetPasquetta(2019);
            Assert.AreEqual(pasquetta, new DateTime(2019, 4, 22));
            pasquetta = DateTimeExtensions.GetPasquetta(2020);
            Assert.AreEqual(pasquetta, new DateTime(2020, 4, 13));
            pasquetta = DateTimeExtensions.GetEasterMonday(2021);
            Assert.AreEqual(pasquetta, new DateTime(2021, 4, 5));
        }

        [Test, Description("Get Business Days Tests")]
        public void TestGetBusinessDays()
        {
            int businessDays;

            //With 1 weekend and holiday
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 09),
                                                              new DateTime(2020, 04, 14),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 3);
            //With 1 weekend and holiday - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 14),
                                                              new DateTime(2020, 04, 09),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -3);
            //With 1 weekend
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 15),
                                                              new DateTime(2020, 04, 22),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 6);
            //With 1 weekend - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 22),
                                                              new DateTime(2020, 04, 15),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -6);
            //With 2 weekends
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 15),
                                                              new DateTime(2020, 04, 29),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 11);
            //With 2 weekends - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 29),
                                                              new DateTime(2020, 04, 15),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -11);
            //No weekends
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 14),
                                                              new DateTime(2020, 04, 17),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 4);
            //No weekends - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 17),
                                                              new DateTime(2020, 04, 14),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -4);
            //Starting on a weekend
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 19),
                                                              new DateTime(2020, 04, 24),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 5);
            //Starting on a weekend - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 24),
                                                              new DateTime(2020, 04, 19),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -5);
            //Ending on a weekend
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 20),
                                                              new DateTime(2020, 04, 25),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 5);
            //Ending on a weekend - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 25),
                                                              new DateTime(2020, 04, 20),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -5);
            //Starting and ending on the same weekend
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 25),
                                                              new DateTime(2020, 04, 26),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 0);
            //Starting and ending on the same weekend - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 26),
                                                              new DateTime(2020, 04, 25),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 0);
            //Starting and ending on different weekends
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 19),
                                                              new DateTime(2020, 04, 25),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 5);
            //Starting and ending on different weekends
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 25),
                                                              new DateTime(2020, 04, 19),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -5);
            //Starting on a holiday
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 02, 17),
                                                              new DateTime(2020, 02, 21),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 4);
            //Starting on a holiday - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 02, 21),
                                                              new DateTime(2020, 02, 17),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -4);
            //Ending on a holiday
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 06),
                                                              new DateTime(2020, 04, 10),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 4);
            //Ending on a holiday - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 10),
                                                              new DateTime(2020, 04, 06),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -4);
            //Starting at a holiday and ending on the same weekend
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 10),
                                                              new DateTime(2020, 04, 12),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 0);
            //Starting at a holiday and ending on the same weekend - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 12),
                                                              new DateTime(2020, 04, 10),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 0);
            //Starting at a weekend and ending on a holiday immediately after the weekend
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 05, 23),
                                                              new DateTime(2020, 05, 25),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 0);
            //Starting at a weekend and ending on a holiday immediately after the weekend - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 05, 25),
                                                              new DateTime(2020, 05, 23),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 0);
            //Starting on a holiday, ending on a weekend
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 02, 17),
                                                              new DateTime(2020, 02, 23),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 4);
            //Starting on a holiday, ending on a weekend - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 02, 23),
                                                              new DateTime(2020, 02, 17),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -4);
            //Starting on a weekend, ending on a holiday
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 04),
                                                              new DateTime(2020, 04, 10),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 4);
            //Starting on a weekend, ending on a holiday - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 10),
                                                              new DateTime(2020, 04, 04),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -4);
            //From an year to another, with correct holiday capture
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 12, 28),
                                                              new DateTime(2021, 01, 08),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 9);
            //From an year to another, with correct holiday capture - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2021, 01, 08),
                                                              new DateTime(2020, 12, 28),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -9);
            //From an year to another, with wrong holiday capture
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 12, 28),
                                                              new DateTime(2021, 01, 08),
                                                              DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                             testList));
            Assert.AreNotEqual(businessDays, 9);
            Assert.AreEqual(businessDays, 10);
            //From an year to another, with wrong holiday capture - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2021, 01, 08),
                                                              new DateTime(2020, 12, 28),
                                                              DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                             testList));
            Assert.AreNotEqual(businessDays, -9);
            Assert.AreEqual(businessDays, -10);
            //From an year to another, with correct holiday capture
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2019, 12, 23),
                                                              new DateTime(2020, 01, 03),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 8);
            //From an year to another, with correct holiday capture - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 01, 03),
                                                              new DateTime(2019, 12, 23),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -8);
            //From an year to another, with wrong holiday capture
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2019, 12, 23),
                                                              new DateTime(2020, 01, 03),
                                                              DateTimeExtensions.GetHolidays(new List<int> { 2019 }.AsEnumerable(),
                                                                                             testList));
            Assert.AreNotEqual(businessDays, 8);
            Assert.AreEqual(businessDays, 9);
            //From an year to another, with wrong holiday capture - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 01, 03),
                                                              new DateTime(2019, 12, 23),
                                                              DateTimeExtensions.GetHolidays(new List<int> { 2019 }.AsEnumerable(),
                                                                                             testList));
            Assert.AreNotEqual(businessDays, -8);
            Assert.AreEqual(businessDays, -9);
            //From an year to another, with wrong holiday capture
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2019, 12, 23),
                                                              new DateTime(2020, 01, 03),
                                                              DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                             testList));
            Assert.AreNotEqual(businessDays, 8);
            Assert.AreEqual(businessDays, 9);
            //From an year to another, with wrong holiday capture - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 01, 03),
                                                              new DateTime(2019, 12, 23),
                                                              DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                             testList));
            Assert.AreNotEqual(businessDays, -8);
            Assert.AreEqual(businessDays, -9);
            //From an year to another, with no holiday capture
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2019, 12, 23),
                                                              new DateTime(2020, 01, 03),
                                                              null);
            Assert.AreNotEqual(businessDays, 8);
            Assert.AreEqual(businessDays, 10);
            //From an year to another, with no holiday capture - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 01, 03),
                                                              new DateTime(2019, 12, 23),
                                                              null);
            Assert.AreNotEqual(businessDays, -8);
            Assert.AreEqual(businessDays, -10);
            //With 1 weekend and holiday, with no holiday capture
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 09),
                                                              new DateTime(2020, 04, 14),
                                                              null);
            Assert.AreNotEqual(businessDays, 3);
            Assert.AreEqual(businessDays, 4);
            //With 1 weekend and holiday, with no holiday capture - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 14),
                                                              new DateTime(2020, 04, 09),
                                                              null);
            Assert.AreNotEqual(businessDays, -3);
            Assert.AreEqual(businessDays, -4);
            //With 1 weekend and holiday, and start date in the end of day
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 09, 23, 59, 59),
                                                              new DateTime(2020, 04, 14),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 3);
            //With 1 weekend and holiday, and start date in the end of day - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 14),
                                                              new DateTime(2020, 04, 09, 23, 59, 59),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -3);
            //With 1 weekend and holiday, and end date in the end of day
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 09),
                                                              new DateTime(2020, 04, 14, 23, 59, 59),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, 3);
            //With 1 weekend and holiday, and end date  in the end of day - reversing dates
            businessDays = DateTimeExtensions.GetBusinessDays(new DateTime(2020, 04, 14, 23, 59, 59),
                                                              new DateTime(2020, 04, 09),
                                                              yearHolidays);
            Assert.AreEqual(businessDays, -3);
        }

        [Test, Description("Add Business Days Tests")]
        public void TestAddBusinessDays()
        {
            DateTime resultDate;

            //Add normal business days
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 04, 27),
                                                            3,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 30));
            //Add business days beyond a weekend
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 04, 17),
                                                            1,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 20));
            //Add business days beyond a holiday
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 11, 25),
                                                            1,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 11, 27));
            //Add business days beyond a holiday and a weekend
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 04, 08),
                                                            3,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 14));
            //Add business days beyond a weekend and a holiday
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 05, 21),
                                                            3,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 05, 27));
            //Add negative business days
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 04, 23),
                                                            -3,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 20));
            //Add business days beyond new year
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 12, 30),
                                                            3,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2021, 01, 05));
            //Add business days beyond new year, with wrong holiday table
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 12, 30),
                                                            3,
                                                            DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                           testList));
            Assert.AreNotEqual(resultDate, new DateTime(2021, 01, 05));
            Assert.AreEqual(resultDate, new DateTime(2021, 01, 04));
            //Add business days beyond Christmas and new year
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 12, 23),
                                                            10,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2021, 01, 08));
            //Add business days beyond Christmas and new year, with wrong holiday table
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 12, 23),
                                                            10,
                                                            DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                           testList));
            Assert.AreNotEqual(resultDate, new DateTime(2021, 01, 08));
            Assert.AreEqual(resultDate, new DateTime(2021, 01, 07));
            //Add business days beyond Christmas and new year, with wrong holiday table
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 12, 23),
                                                            10,
                                                            DateTimeExtensions.GetHolidays(new List<int> { 2021 }.AsEnumerable(),
                                                                                           testList));
            Assert.AreNotEqual(resultDate, new DateTime(2021, 01, 08));
            Assert.AreEqual(resultDate, new DateTime(2021, 01, 07));
            //Add business days beyond Christmas and new year, with no holiday table
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 12, 23),
                                                            10,
                                                            null);
            Assert.AreNotEqual(resultDate, new DateTime(2021, 01, 08));
            Assert.AreEqual(resultDate, new DateTime(2021, 01, 06));
            //Add business days beyond a holiday and a weekend, with no holiday table
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 04, 08),
                                                            3,
                                                            null);
            Assert.AreNotEqual(resultDate, new DateTime(2020, 04, 14));
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 13));
            //Add no business days
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 04, 29),
                                                            0,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 29));
            //Add business days to a date with time included
            resultDate = DateTimeExtensions.AddBusinessDays(new DateTime(2020, 04, 27, 09, 30, 00),
                                                            3,
                                                            yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 30, 09, 30, 00));
        }

        [Test, Description("Subtract Business Days Tests")]
        public void TestSubtractBusinessDays()
        {
            DateTime resultDate;

            //Subtract normal business days
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 04, 30),
                                                                 3,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 27));
            //Subtract business days beyond a weekend
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 04, 20),
                                                                 1,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 17));
            //Subtract business days beyond a holiday
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 11, 27),
                                                                 1,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 11, 25));
            //Subtract business days beyond a holiday and a weekend
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 04, 14),
                                                                 3,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 08));
            //Subtract business days beyond a weekend and a holiday
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 05, 27),
                                                                 3,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 05, 21));
            //Subtract negative business days
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 04, 20),
                                                                 -3,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 23));
            //Subtract business days from new year
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2021, 01, 05),
                                                                 3,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 12, 30));
            //Subtract business days from new year, with wrong holiday table
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2021, 01, 05),
                                                                 3,
                                                                 DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                                testList));
            Assert.AreNotEqual(resultDate, new DateTime(2020, 12, 30));
            Assert.AreEqual(resultDate, new DateTime(2020, 12, 31));
            //Subtract business days including Christmas and new year
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2021, 01, 08),
                                                                 10,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 12, 23));
            //Subtract business days that should include Christmas and new year, with wrong holiday table
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2021, 01, 08),
                                                                 10,
                                                                 DateTimeExtensions.GetHolidays(new List<int> { 2020 }.AsEnumerable(),
                                                                                                testList));
            Assert.AreNotEqual(resultDate, new DateTime(2020, 12, 23));
            Assert.AreEqual(resultDate, new DateTime(2020, 12, 24));
            //Subtract business days that should include Christmas and new year, with wrong holiday table
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2021, 01, 08),
                                                                 10,
                                                                 DateTimeExtensions.GetHolidays(new List<int> { 2021 }.AsEnumerable(),
                                                                                                testList));
            Assert.AreNotEqual(resultDate, new DateTime(2020, 12, 23));
            Assert.AreEqual(resultDate, new DateTime(2020, 12, 24));
            //Subtract business days that should include Christmas and new year, with no holiday table
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2021, 01, 08),
                                                                 10,
                                                                 null);
            Assert.AreNotEqual(resultDate, new DateTime(2020, 12, 23));
            Assert.AreEqual(resultDate, new DateTime(2020, 12, 25));
            //Subtract business days beyond a holiday and a weekend, with no holiday table
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 04, 14),
                                                                 3,
                                                                 null);
            Assert.AreNotEqual(resultDate, new DateTime(2020, 04, 08));
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 09));
            //Subtract no business days
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 04, 29),
                                                                 0,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 29));
            //Subtract business days to a date with time included
            resultDate = DateTimeExtensions.SubtractBusinessDays(new DateTime(2020, 04, 30, 09, 30, 00),
                                                                 3,
                                                                 yearHolidays);
            Assert.AreEqual(resultDate, new DateTime(2020, 04, 27, 09, 30, 00));
        }
    }
}