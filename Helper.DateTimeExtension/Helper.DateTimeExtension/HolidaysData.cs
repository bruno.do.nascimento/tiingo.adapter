﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace Helper.DateTimeExtension
{
    public class HolidaysData
    {
        //private const string DATA_CONNECTION = "Data Source=tcp:34.66.246.125;Initial Catalog=StockModel;Persist Security Info=True;User ID=stock;Password=U54S70ckM4rk375";
        private const string DATA_CONNECTION = "Server=feup-cm-tiingo-services.database.windows.net;Initial Catalog=GlobalMarket;User ID=brunonascimento;Password=U54570ckM4rk375;Connect Timeout=60;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private const string HOLIDAYS_QUERY = "SELECT y.year year, (SELECT holiday, observance FROM dbo.holidays_table(y.year, 23) ORDER BY holiday FOR JSON PATH, INCLUDE_NULL_VALUES) holidays FROM dbo.SM_holidays y GROUP BY y.year";

        private static SortedDictionary<int, IDictionary<DateTime, string>> yearHolidays = null;

        public static async Task LoadHolidaysData()
        {
            SqlConnection connection;
            SqlCommand command;
            SqlDataReader holidaysResult;

            int year;
            JArray holidaysJSON;

            if (YearHolidays == null)
                YearHolidays = new SortedDictionary<int, IDictionary<DateTime, string>>();

            if (YearHolidays.Count < 1)
            {
                connection = new SqlConnection(DATA_CONNECTION);
                try
                {
                    await connection.OpenAsync();
                    command = new SqlCommand(HOLIDAYS_QUERY, connection);
                    holidaysResult = await command.ExecuteReaderAsync();

                    while (await holidaysResult.ReadAsync())
                    {
                        year = (int)holidaysResult["year"];
                        holidaysJSON = JArray.Parse((string)holidaysResult["holidays"]);

                        SortedDictionary<DateTime, string> yearHolidayList = new SortedDictionary<DateTime, string>();
                        foreach (JToken holiday in holidaysJSON)
                        {
                            yearHolidayList.Add(ConversionHelper.ConvertStringDate((string)holiday.SelectToken("holiday"),
                                                                                   ConversionHelper.LONGDATE_FORMAT),
                                                (string)holiday.SelectToken("observance"));
                        }
                        YearHolidays.Add(year, yearHolidayList);
                    }

                    await connection.CloseAsync();
                }
                catch (ArgumentException aExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", aExcp.Message, aExcp.StackTrace);
                }
                catch (DeletedRowInaccessibleException driExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", driExcp.Message, driExcp.StackTrace);
                }
                catch (InvalidCastException icExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", icExcp.Message, icExcp.StackTrace);
                }
                catch (NoNullAllowedException nnaExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", nnaExcp.Message, nnaExcp.StackTrace);
                }
                catch (ObjectDisposedException odExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", odExcp.Message, odExcp.StackTrace);
                }
                catch (InvalidOperationException iopExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", iopExcp.Message, iopExcp.StackTrace);
                }
                catch (SqlException sqlExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", sqlExcp.Message, sqlExcp.StackTrace);
                }
                catch (IOException ioExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", ioExcp.Message, ioExcp.StackTrace);
                }
                catch (ConfigurationErrorsException cerrExcp)
                {
                    Console.WriteLine("Message: {0} Stack: {1}", cerrExcp.Message, cerrExcp.StackTrace);
                }
            }
        }
        public static SortedDictionary<int, IDictionary<DateTime, string>> YearHolidays { get => yearHolidays; set => yearHolidays = value; }
    }
}
