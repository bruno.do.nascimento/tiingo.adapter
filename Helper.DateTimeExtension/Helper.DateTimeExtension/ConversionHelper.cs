﻿using System;
using System.Globalization;

namespace Helper.DateTimeExtension
{
    public class ConversionHelper
    {
        public const string SERVICE_SHORTDATE_FORMAT = "yyyyMMdd";
        public const string SHORTDATE_FORMAT = "yyyy-MM-dd";
        public const string LONGDATE_FORMAT = "yyyy-MM-ddTHH:mm:ss";
        public const string MILISECOND_FORMAT = "yyyy-MM-ddTHH:mm:ss.fff";
        public const string TIMEZONE_FORMAT = "yyyy-MM-ddTHH:mm:sszzz";
        public const string OFFSET_FORMAT = "zzz";
        public const string TIMESTAMP_FORMAT = "yyyy-MM-ddTHH:mm:ss.fffffffzzz";
        public const string NODA_TIMESTAMP_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss;FFFFFFFFFz";

        public const string AZORES = "Azores Standard Time";             // Açores Timezone
        public const string GMT = "GMT Standard Time";                   // GMT - Portugal Timezone
        public const string WEST = "W. Europe Standard Time";            // WEST - Western Europe Timezone
        public const string EST = "Eastern Standard Time";               // EST - Eastern Standard Timezone
        public const string CST = "Central Standard Time";               // CST - Central Standard Timezone
        public const string MST = "Mountain Standard Time";              // MST - Mountain Standard Timezone
        public const string PST = "Pacific Standard Time";               // PST - Pacific Standard Timezone
        public const string BRASILIA = "E. South America Standard Time"; // Brasília Timezone

        public static DateTimeOffset ConvertStringTimeStamp(string timestamp, string format)
        {
            CultureInfo culture = (CultureInfo)new CultureInfo("en-US");
            DateTimeFormatInfo formatInfo = culture.DateTimeFormat;
            formatInfo.LongTimePattern = format;

            return DateTimeOffset.Parse(timestamp, formatInfo, DateTimeStyles.AssumeLocal);
        }

        public static DateTime ConvertStringDate(string date, string format)
        {
            CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            DateTimeFormatInfo formatInfo = culture.DateTimeFormat;
            formatInfo.ShortTimePattern = format;

            return DateTime.Parse(date, formatInfo);
        }
    }
}
