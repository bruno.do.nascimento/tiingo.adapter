﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Helper.DateTimeExtension
{
    /// <summary>
    /// Helper/extension class for manipulating date and time values.
    /// </summary>
    public static class DateTimeExtensions
    {
        public static DateTime AddBusinessDays(this DateTime current,
                                               int days,
                                               IEnumerable<DateTime> holidays = null)
        {
            int sign = Math.Sign(days);
            int unsignedDays = Math.Abs(days);
            for (int i = 0; i < unsignedDays; i++)
            {
                do
                {
                    current = current.AddDays(sign);
                }
                while (current.DayOfWeek == DayOfWeek.Saturday ||
                       current.DayOfWeek == DayOfWeek.Sunday ||
                       (holidays != null && holidays.Contains(current.Date))
                      );
            }

            return current;
        }

        /// <summary>
        /// Subtracts the given number of business days to the <see cref="DateTime"/>.
        /// </summary>
        /// <param name="current">The date to be changed.</param>
        /// <param name="days">Number of business days to be subtracted.</param>
        /// <param name="holidays">An optional list of holiday (non-business) days to consider.</param>
        /// <returns>A <see cref="DateTime"/> increased by a given number of business days.</returns>
        public static DateTime SubtractBusinessDays(this DateTime current,
                                                    int days,
                                                    IEnumerable<DateTime> holidays)
        {
            return DateTimeExtensions.AddBusinessDays(current, -days, holidays);
        }

        /// <summary>
        /// Retrieves the number of business days from two dates
        /// </summary>
        /// <param name="startDate">The inclusive start date</param>
        /// <param name="endDate">The inclusive end date</param>
        /// <param name="holidays">An optional list of holiday (non-business) days to consider.</param>
        /// <returns></returns>
        public static int GetBusinessDays(this DateTime startDate,
                                          DateTime endDate,
                                          IEnumerable<DateTime> holidays)
        {
            int sign = Math.Sign((endDate.Date - startDate.Date).Days);

            int cnt = 0;
            for (DateTime current = startDate.Date; current != endDate.Date; current = current.AddDays(sign))
            {
                if (current.DayOfWeek == DayOfWeek.Saturday ||
                    current.DayOfWeek == DayOfWeek.Sunday ||
                    (holidays != null && holidays.Contains(current.Date))
                   )
                {
                    // skip weekends and holidays 
                }
                else
                {
                    cnt += sign;
                }
            }

            if (endDate.DayOfWeek == DayOfWeek.Saturday ||
                endDate.DayOfWeek == DayOfWeek.Sunday ||
                (holidays != null && holidays.Contains(endDate.Date)))
            {
                //don't count another day
            }
            else
            {
                //count the last day
                cnt += sign;
            }

            return cnt;
        }

        /// <summary>
        /// Calculate Easter Sunday for any given year.
        /// src.: https://stackoverflow.com/a/2510411/1233379
        /// </summary>
        /// <param name="year">The year to calculate Easter against.</param>
        /// <returns>a DateTime object containing the Easter month and day for the given year</returns>
        public static DateTime GetEasterSunday(int year)
        {
            int day = 0;
            int month = 0;

            int g = year % 19;
            int c = year / 100;
            int h = (c - (int)(c / 4) - (int)((8 * c + 13) / 25) + 19 * g + 15) % 30;
            int i = h - (int)(h / 28) * (1 - (int)(h / 28) * (int)(29 / (h + 1)) * (int)((21 - g) / 11));

            day = i - ((year + (int)(year / 4) + i + 2 - c + (int)(c / 4)) % 7) + 28;
            month = 3;

            if (day > 31)
            {
                month++;
                day -= 31;
            }

            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Calculate Good Friday for any given year, based on Easter Sunday.
        /// </summary>
        /// <param name="year">The year to calculate Easter against.</param>
        /// <returns>a DateTime object containing the Good Friday for the given year</returns>
        public static DateTime GetGoodFriday(int year)
        {
            return DateTimeExtensions.GetEasterSunday(year).AddDays(-2);
        }

        /// <summary>
        /// Calculate Easter Monday for any given year, based on Easter Sunday.
        /// </summary>
        /// <param name="year">The year to calculate Easter against.</param>
        /// <returns>a DateTime object containing the Easter Monday for the given year</returns>
        public static DateTime GetEasterMonday(int year)
        {
            return DateTimeExtensions.GetEasterSunday(year).AddDays(1);
        }

        /// <summary>
        /// Calculate Pasquetta for any given year, based on Easter Sunday.
        /// </summary>
        /// <param name="year">The year to calculate Easter against.</param>
        /// <returns>a DateTime object containing the Easter Monday for the given year</returns>
        public static DateTime GetPasquetta(int year)
        {
            return DateTimeExtensions.GetEasterMonday(year);
        }

        /// <summary>
        /// Retrieve holidays from a database
        /// </summary>
        /// <returns></returns>
        public static IDictionary<int, IDictionary<DateTime, string>> LoadHolidays()
        {
            Mutex mut = new Mutex();
            SortedDictionary<int, IDictionary<DateTime, string>> allHolidays;

            mut.WaitOne();
            HolidaysData.LoadHolidaysData().Wait();
            allHolidays = HolidaysData.YearHolidays;
            mut.ReleaseMutex();

            return allHolidays;
        }

        /// <summary>
        /// Retrieve holidays for given years
        /// </summary>
        /// <param name="years">an array of years to retrieve the holidays</param>
        /// <returns>a list with all the holidays of the given years</returns>
        public static IEnumerable<DateTime> GetHolidays(IEnumerable<int> years, IDictionary<int, IDictionary<DateTime, string>> holidayList)
        {
            List<DateTime> lst = new List<DateTime>();

            foreach (int year in years.Distinct().OrderBy(year => year))
            {
                lst.AddRange(holidayList[year].Keys);
            }
            return lst;
        }
    }
}
