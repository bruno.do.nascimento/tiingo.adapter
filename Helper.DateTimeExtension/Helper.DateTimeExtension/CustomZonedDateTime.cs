﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Helper.DateTimeExtension
{
    [Serializable()]
    [JsonConverter(typeof(ZonedDateTimeConverter))]
    public class CustomZonedDateTime
    {
        private DateTimeOffset date;
        private long nanoseconds;

        public CustomZonedDateTime()
        {
        }

        public CustomZonedDateTime(DateTimeOffset date, 
                                   long nanoseconds)
        {
            Date = date;
            Nanoseconds = nanoseconds;
        }

        public override string ToString()
        {
            return ConversionHelper.ConvertCustomZonedDatetimeString(this);
        }
        public override bool Equals(object? obj)
        {
            bool resultObj = obj is CustomZonedDateTime time && date.Equals(time.date) && nanoseconds == time.nanoseconds;
            bool resultDate = obj is DateTimeOffset ztime && date.Equals(ztime) && nanoseconds == 0;

            return resultObj || resultDate;
        }

        public override int GetHashCode()
        {
            return date.GetHashCode();
        }

        public static implicit operator DateTimeOffset(CustomZonedDateTime c) => c.Date;
        public static implicit operator CustomZonedDateTime (DateTimeOffset z) => new CustomZonedDateTime(z, z.Ticks);
        public DateTimeOffset Date { get => date; set => date = value; }
        public long Nanoseconds { get => nanoseconds; set => nanoseconds = value; }
    }
}
