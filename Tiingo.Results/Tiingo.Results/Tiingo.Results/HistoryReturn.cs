﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using Tiingo.Metadata;
using Tiingo.WebCode;

namespace Tiingo.Results
{
    public class HistoryReturn
    {
        private WebStatus status = new WebStatus();
        private StockMetadata? tickerData = new StockMetadata();
        private List<StockHistory?> priceList = new List<StockHistory?>();

        public HistoryReturn()
        {

        }

        public HistoryReturn(WebStatus status,
                             StockMetadata? tickerData,
                             List<StockHistory?> priceList)
        {
            Status = status;
            TickerData = tickerData;
            PriceList = priceList;
        }

        public HistoryReturn(HttpStatusCode httpStatus,
                             string? content,
                             JObject tickerToken,
                             JArray priceToken)
        {
            Status = new WebStatus(httpStatus, (content ?? WebStatus.MSG_HTTPOK));
            TickerData = new StockMetadata((JToken)tickerToken);
            if (httpStatus == HttpStatusCode.OK)
            {
                foreach (JToken priceResult in priceToken)
                {
                    PriceList.Add(new StockHistory(priceResult));
                }
            }
        }

        public WebStatus Status { get => status; set => status = value; }
        public StockMetadata? TickerData { get => tickerData; set => tickerData = value; }
        public List<StockHistory?> PriceList { get => priceList; set => priceList = value; }
    }
}
