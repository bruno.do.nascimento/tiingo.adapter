﻿using Helper.DateTimeExtension;
using Newtonsoft.Json.Linq;
using System;

namespace Tiingo.Results
{
    public class IntradayQuote
    {
        private DateTimeOffset date;
        private double? close;
        private double? high;
        private double? low;
        private double? open;

        public IntradayQuote()
        {
        }

        public IntradayQuote(DateTimeOffset date,
                             double? close,
                             double? high,
                             double? low,
                             double? open)
        {
            Date = date;
            Close = close;
            High = high;
            Low = low;
            Open = open;
        }

        public IntradayQuote(JToken token)
        {
            try
            {
                Date = TimeZoneInfo.ConvertTime((DateTimeOffset)token.SelectToken("date"),
                                                TimeZoneInfo.FindSystemTimeZoneById(ConversionHelper.EST));
            }
            catch (TimeZoneNotFoundException tznfExcp)
            {
                Date = (DateTimeOffset)token.SelectToken("date");
                Console.WriteLine("Message: {0} Stack: {1}", tznfExcp.Message, tznfExcp.StackTrace);
            }
            catch (InvalidTimeZoneException itzExcp)
            {
                Date = (DateTimeOffset)token.SelectToken("date");
                Console.WriteLine("Message: {0} Stack: {1}", itzExcp.Message, itzExcp.StackTrace);
            }
            Close = (double?)token.SelectToken("close");
            High = (double?)token.SelectToken("high");
            Low = (double?)token.SelectToken("low");
            Open = (double?)token.SelectToken("open");
        }

        public DateTimeOffset Date { get => date; set => date = value; }
        public double? Close { get => close; set => close = value; }
        public double? High { get => high; set => high = value; }
        public double? Low { get => low; set => low = value; }
        public double? Open { get => open; set => open = value; }
    }
}
