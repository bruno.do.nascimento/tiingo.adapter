﻿using Newtonsoft.Json.Linq;
using System;

namespace Tiingo.Results
{
    public class StockHistory
    {
        private DateTime date;
        private double? close;
        private double? high;
        private double? low;
        private double? open;
        private long? volume;
        private double? adjClose;
        private double? adjHigh;
        private double? adjLow;
        private double? adjOpen;
        private long? adjVolume;
        private double? divCash;
        private double? splitFactor;

        public StockHistory()
        {
        }

        public StockHistory(DateTime date,
                            double? close,
                            double? high,
                            double? low,
                            double? open,
                            long? volume,
                            double? adjClose,
                            double? adjHigh,
                            double? adjLow,
                            double? adjOpen,
                            long? adjVolume,
                            double? divCash,
                            double? splitFactor)
        {
            Date = date;
            Close = close;
            High = high;
            Low = low;
            Open = open;
            Volume = volume;
            AdjClose = adjClose;
            AdjHigh = adjHigh;
            AdjLow = adjLow;
            AdjOpen = adjOpen;
            AdjVolume = adjVolume;
            DivCash = divCash;
            SplitFactor = splitFactor;
        }

        public StockHistory(JToken token)
        {
            Date = (DateTime)token.SelectToken("date");
            Close = (double?)token.SelectToken("close");
            High = (double?)token.SelectToken("high");
            Low = (double?)token.SelectToken("low");
            Open = (double?)token.SelectToken("open");
            Volume = (long?)token.SelectToken("volume");
            AdjClose = (double?)token.SelectToken("adjClose");
            AdjHigh = (double?)token.SelectToken("adjHigh");
            AdjLow = (double?)token.SelectToken("adjLow");
            AdjOpen = (double?)token.SelectToken("adjOpen");
            AdjVolume = (long?)token.SelectToken("adjVolume");
            DivCash = (double?)token.SelectToken("divCash");
            SplitFactor = (double?)token.SelectToken("splitFactor");
        }

        public DateTime Date { get => date; set => date = value; }
        public double? Close { get => close; set => close = value; }
        public double? High { get => high; set => high = value; }
        public double? Low { get => low; set => low = value; }
        public double? Open { get => open; set => open = value; }
        public long? Volume { get => volume; set => volume = value; }
        public double? AdjClose { get => adjClose; set => adjClose = value; }
        public double? AdjHigh { get => adjHigh; set => adjHigh = value; }
        public double? AdjLow { get => adjLow; set => adjLow = value; }
        public double? AdjOpen { get => adjOpen; set => adjOpen = value; }
        public long? AdjVolume { get => adjVolume; set => adjVolume = value; }
        public double? DivCash { get => divCash; set => divCash = value; }
        public double? SplitFactor { get => splitFactor; set => splitFactor = value; }
    }
}
