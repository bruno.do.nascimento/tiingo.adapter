﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using Tiingo.WebCode;

namespace Tiingo.Results
{
    public class StockQuote
    {
        private WebStatus status;
        private SortedDictionary<string, LastQuote> quoteList = new SortedDictionary<string, LastQuote>();

        public StockQuote()
        {
        }

        public StockQuote(WebStatus status, SortedDictionary<string, LastQuote> quoteList)
        {
            Status = status;
            QuoteList = quoteList;
        }

        public StockQuote(HttpStatusCode httpStatus,
                          string content,
                          JArray tobToken)
        {
            string ticker;
            LastQuote tobQuote;

            Status = new WebStatus(httpStatus, (content ?? WebStatus.MSG_HTTPOK));
            if (httpStatus == HttpStatusCode.OK)
            {
                foreach (JToken tobResult in tobToken)
                {
                    ticker = (string)tobResult.SelectToken("ticker") ?? throw new ArgumentNullException(nameof(ticker));
                    tobQuote = new LastQuote(tobResult);
                    QuoteList.Add(ticker, tobQuote);
                }
            }
        }

        public WebStatus Status { get => status; set => status = value; }
        public SortedDictionary<string, LastQuote> QuoteList { get => quoteList; set => quoteList = value; }
    }
}
