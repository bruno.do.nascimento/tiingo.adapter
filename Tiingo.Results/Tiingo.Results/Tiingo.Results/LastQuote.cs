﻿using Newtonsoft.Json.Linq;
using System;

namespace Tiingo.Results
{
    public class LastQuote
    {
        private string ticker;
        private DateTimeOffset quoteTimestamp;
        private double? high;
        private double? low;
        private double? open;
        private double? last;
        private double? prevClose;
        private double? askPrice;
        private double? bidPrice;
        private double? mid;
        private double? tngoLast;
        private long? lastSize;
        private long? askSize;
        private long? bidSize;
        private DateTimeOffset timestamp;
        private long? volume;
        private DateTimeOffset lastSaleTimestamp;

        public LastQuote()
        {
        }

        public LastQuote(string ticker,
                         DateTimeOffset quoteTimestamp,
                         double? high,
                         double? low,
                         double? open,
                         double? last,
                         double? prevClose,
                         double? askPrice,
                         double? bidPrice,
                         double? mid,
                         double? tngoLast,
                         long? lastSize,
                         long? askSize,
                         long? bidSize,
                         DateTimeOffset timestamp,
                         long? volume,
                         DateTimeOffset lastSaleTimestamp)
        {
            Ticker = ticker ?? throw new ArgumentNullException(nameof(ticker));
            QuoteTimestamp = quoteTimestamp;
            High = high;
            Low = low;
            Open = open;
            Last = last;
            PrevClose = prevClose;
            AskPrice = askPrice;
            BidPrice = bidPrice;
            Mid = mid;
            TngoLast = tngoLast;
            LastSize = lastSize;
            AskSize = askSize;
            BidSize = bidSize;
            Timestamp = timestamp;
            Volume = volume;
            LastSaleTimestamp = lastSaleTimestamp;
        }

        public LastQuote(JToken token)
        {
            Ticker = (string)token.SelectToken("ticker") ?? throw new ArgumentNullException(nameof(ticker));
            QuoteTimestamp = ((DateTimeOffset)token.SelectToken("quoteTimestamp"));
            High = (double?)token.SelectToken("high");
            Low = (double?)token.SelectToken("low");
            Open = (double?)token.SelectToken("open");
            Last = (double?)token.SelectToken("last");
            PrevClose = (double?)token.SelectToken("prevClose");
            AskPrice = (double?)token.SelectToken("askPrice");
            BidPrice = (double?)token.SelectToken("bidPrice");
            Mid = (double?)token.SelectToken("mid");
            TngoLast = (double?)token.SelectToken("tngoLast");
            LastSize = (long?)token.SelectToken("lastSize");
            AskSize = (long?)token.SelectToken("askSize");
            BidSize = (long?)token.SelectToken("bidSize");
            Timestamp = (DateTimeOffset)token.SelectToken("timestamp");
            Volume = (long?)token.SelectToken("volume");
            LastSaleTimestamp = (DateTimeOffset)token.SelectToken("lastSaleTimestamp");
        }

        public string Ticker { get => ticker; set => ticker = value; }
        public DateTimeOffset QuoteTimestamp { get => quoteTimestamp; set => quoteTimestamp = value; }
        public double? High { get => high; set => high = value; }
        public double? Low { get => low; set => low = value; }
        public double? Open { get => open; set => open = value; }
        public double? PrevClose { get => prevClose; set => prevClose = value; }
        public double? AskPrice { get => askPrice; set => askPrice = value; }
        public double? BidPrice { get => bidPrice; set => bidPrice = value; }
        public double? Mid { get => mid; set => mid = value; }
        public double? TngoLast { get => tngoLast; set => tngoLast = value; }
        public long? LastSize { get => lastSize; set => lastSize = value; }
        public long? AskSize { get => askSize; set => askSize = value; }
        public DateTimeOffset Timestamp { get => timestamp; set => timestamp = value; }
        public long? Volume { get => volume; set => volume = value; }
        public DateTimeOffset LastSaleTimestamp { get => lastSaleTimestamp; set => lastSaleTimestamp = value; }
        public double? Last { get => last; set => last = value; }
        public long? BidSize { get => bidSize; set => bidSize = value; }
    }
}
