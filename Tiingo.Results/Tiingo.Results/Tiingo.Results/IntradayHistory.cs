﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using Tiingo.Metadata;
using Tiingo.WebCode;

namespace Tiingo.Results
{
    public class IntradayHistory
    {
        private WebStatus status;
        private StockMetadata stock;
        private List<IntradayQuote> intraday = new List<IntradayQuote>();

        public IntradayHistory()
        {
        }

        public IntradayHistory(WebStatus status, StockMetadata stock, List<IntradayQuote> intraDay)
        {
            Status = status;
            Stock = stock;
            Intraday = intraDay;
        }

        public IntradayHistory(HttpStatusCode code,
                               string message,
                               JToken metadata,
                               JArray allIntradayQuotes)
        {
            Status = new WebStatus(code, (message ?? WebStatus.MSG_HTTPOK));
            Stock = new StockMetadata(metadata);
            foreach (JToken intradayQuote in allIntradayQuotes)
            {
                intraday.Add(new IntradayQuote(intradayQuote));
            }
        }

        public WebStatus Status { get => status; set => status = value; }
        public StockMetadata Stock { get => stock; set => stock = value; }
        public List<IntradayQuote> Intraday { get => intraday; set => intraday = value; }
    }
}
