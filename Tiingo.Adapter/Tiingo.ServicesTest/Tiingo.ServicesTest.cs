using NUnit.Framework;
using System;
using System.Collections.Generic;
using Tiingo.Cryptocurrency;
using Tiingo.Results;
using Tiingo.Services;

namespace Tiingo.ServicesTest
{
    public class TiingoStockServicesTests
    {
        ServiceEndPoints servEnd = null;

        [SetUp]
        public void Setup()
        {
            servEnd = ServiceEndPoints.getServiceEndPoints();
        }

        [Test, Description("Null stock service")]
        public void TestNotNullStockService()
        {
            Assert.IsNotNull(servEnd);
        }

        [Test, Description("Service History calls")]
        public void TestGetHistory()
        {
            List<HistoryReturn> newStockHistory = null;
            List<string> symbolList = null;
            int quotes;
            Exception ex;

            //1 stock, 7 quotes
            symbolList = new List<string> { "AAPL" };
            quotes = 7;
            newStockHistory = servEnd.GetHistory(symbolList, quotes);
            Assert.IsNotNull(newStockHistory);
            Assert.AreEqual(newStockHistory.Count, symbolList.Count);
            Assert.AreEqual(newStockHistory[0].PriceList.Count, quotes);
            //2 stocks, 10 quotes
            newStockHistory = null;
            symbolList = new List<string> { "AAPL", "GOOG" };
            quotes = 10;
            newStockHistory = servEnd.GetHistory(symbolList, quotes);
            Assert.IsNotNull(newStockHistory);
            Assert.AreEqual(newStockHistory.Count, symbolList.Count);
            Assert.AreEqual(newStockHistory[0].PriceList.Count, quotes);
            Assert.AreEqual(newStockHistory[1].PriceList.Count, quotes);
            //3 stocks, 5 quotes
            newStockHistory = null;
            symbolList = new List<string> { "INTL", "AAPL", "GOOG" };
            quotes = 5;
            newStockHistory = servEnd.GetHistory(symbolList, quotes);
            Assert.IsNotNull(newStockHistory);
            Assert.AreEqual(newStockHistory.Count, symbolList.Count);
            Assert.AreEqual(newStockHistory[0].PriceList.Count, quotes);
            Assert.AreEqual(newStockHistory[1].PriceList.Count, quotes);
            Assert.AreEqual(newStockHistory[2].PriceList.Count, quotes);
            //Parameter error - number of quotes
            symbolList = new List<string> { "GOOG" };
            quotes = -1;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetHistory(symbolList, quotes); });
            Assert.AreEqual(ex.Message, "numQuotes");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - null list of symbols
            symbolList = null;
            quotes = 5;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetHistory(symbolList, quotes); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - empty list of symbols
            symbolList = new List<string>();
            quotes = 5;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetHistory(symbolList, quotes); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }

        [Test, Description("Service Quote calls")]
        public void TestGetQuote()
        {
            List<StockQuote> newQuote = null;
            string symbolList = "";
            Exception ex;

            //1 stock
            symbolList = "AAPL";
            newQuote = servEnd.GetQuote(symbolList);
            Assert.IsNotNull(newQuote);
            Assert.IsNotNull(newQuote[0]);
            Assert.AreEqual(newQuote[0].QuoteList.Count, symbolList.Split(",").Length);
            //2 stocks
            symbolList = "IBM,HPQ";
            newQuote = servEnd.GetQuote(symbolList);
            Assert.IsNotNull(newQuote);
            Assert.IsNotNull(newQuote[0]);
            Assert.AreEqual(newQuote[0].QuoteList.Count, symbolList.Split(",").Length);
            //3 stocks
            symbolList = "INTC,GOOG,MSFT";
            newQuote = servEnd.GetQuote(symbolList);
            Assert.IsNotNull(newQuote);
            Assert.IsNotNull(newQuote[0]);
            Assert.AreEqual(newQuote[0].QuoteList.Count, symbolList.Split(",").Length);
            //3 stocks
            symbolList = "INTC, GOOG, MSFT";
            newQuote = servEnd.GetQuote(symbolList);
            Assert.IsNotNull(newQuote);
            Assert.IsNotNull(newQuote[0]);
            Assert.AreEqual(newQuote[0].QuoteList.Count, 1);
            //1 stock with space between it and the symbols tag
            symbolList = " AAPL";
            newQuote = servEnd.GetQuote(symbolList);
            Assert.IsNotNull(newQuote);
            Assert.IsNotNull(newQuote[0]);
            Assert.AreEqual(newQuote[0].QuoteList.Count, 0);
            //null parameter
            symbolList = null;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetQuote(symbolList); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //empty string
            symbolList = "";
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetQuote(symbolList); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }

        [Test, Description("Service Intraday History calls")]
        public void TestGetIntradayHistory()
        {
            const int PER_MINUTE = 391;
            const int PER_FIVE_MINUTES = 79;
            const int PER_THIRTY_MINUTES = 16;
            const int PER_SIXTY_MINUTES = 8;
            const int PER_HOUR = 24;

            const string WRONG_MIN_SYMBOL = "m";
            const string WRONG_HOUR_SYMBOL = "h";

            List<IntradayHistory> newStockIntradayHistory = null;
            List<string> symbolList = null;
            int quotes, timeUnits;
            Exception ex;

            //1 stock, 7 quotes           
            symbolList = new List<string> { "AAPL" };
            quotes = 7;
            newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * quotes * PER_MINUTE);
            //2 stocks, 10 quotes
            symbolList = new List<string> { "AAPL", "GOOG" };
            quotes = 10;
            newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * quotes * PER_MINUTE);
            Assert.IsTrue(newStockIntradayHistory[1].Intraday.Count <= symbolList.Count * quotes * PER_MINUTE);
            //3 stocks, 5 quotes
            symbolList = new List<string> { "INTL", "AAPL", "GOOG" };
            quotes = 5;
            newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * quotes * PER_MINUTE);
            Assert.IsTrue(newStockIntradayHistory[1].Intraday.Count <= symbolList.Count * quotes * PER_MINUTE);
            Assert.IsTrue(newStockIntradayHistory[2].Intraday.Count <= symbolList.Count * quotes * PER_MINUTE);
            //1 stock, 7 quotes, 5 minutes frequency           
            symbolList = new List<string> { "AAPL" };
            quotes = 7;
            timeUnits = 5;
            newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * quotes * PER_FIVE_MINUTES);
            //1 stock, 7 quotes, 30 minutes frequency           
            symbolList = new List<string> { "GOOG" };
            quotes = 7;
            timeUnits = 30;
            newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * quotes * PER_THIRTY_MINUTES);
            //1 stock, 7 quotes, 60 minutes frequency           
            symbolList = new List<string> { "INTL" };
            quotes = 7;
            timeUnits = 60;
            newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * quotes * PER_SIXTY_MINUTES);
            //1 stock, 7 quotes, 1 hour frequency           
            symbolList = new List<string> { "INTL" };
            quotes = 7;
            timeUnits = 1;
            newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.HOUR_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * quotes * PER_HOUR);
            //Parameter error - number of quotes
            symbolList = new List<string> { "GOOG" };
            quotes = -1;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetIntradayHistory(symbolList, quotes); });
            Assert.AreEqual(ex.Message, "numQuotes");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - null list of symbols
            symbolList = null;
            quotes = 5;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetIntradayHistory(symbolList, quotes); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - empty list of symbols
            symbolList = new List<string>();
            quotes = 5;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetIntradayHistory(symbolList, quotes); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - number of time units
            symbolList = new List<string> { "INTL" };
            quotes = 5;
            timeUnits = -1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "numUnits");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (minute)
            symbolList = new List<string> { "AAPL" };
            quotes = 5;
            timeUnits = 1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes, timeUnits, WRONG_MIN_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (hour)
            symbolList = new List<string> { "AAPL" };
            quotes = 5;
            timeUnits = 1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newStockIntradayHistory = servEnd.GetIntradayHistory(symbolList, quotes, timeUnits, WRONG_HOUR_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }

        [Test, Description("Service Intraday Today calls")]
        public void TestGetIntradayToday()
        {
            const int PER_MINUTE = 391;
            const int PER_FIVE_MINUTES = 79;
            const int PER_THIRTY_MINUTES = 16;
            const int PER_SIXTY_MINUTES = 8;
            const int PER_HOUR = 24;

            const string WRONG_MIN_SYMBOL = "m";
            const string WRONG_HOUR_SYMBOL = "h";

            List<IntradayHistory> newStockIntradayHistory = null;
            List<string> symbolList = null;
            int timeUnits;
            Exception ex;

            //1 stock           
            symbolList = new List<string> { "AAPL" };
            newStockIntradayHistory = servEnd.GetIntradayToday(symbolList);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * PER_MINUTE);
            //2 stocks
            symbolList = new List<string> { "AAPL", "GOOG" };
            newStockIntradayHistory = servEnd.GetIntradayToday(symbolList);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * PER_MINUTE);
            Assert.IsTrue(newStockIntradayHistory[1].Intraday.Count <= symbolList.Count * PER_MINUTE);
            //3 stocks
            symbolList = new List<string> { "INTL", "AAPL", "GOOG" };
            newStockIntradayHistory = servEnd.GetIntradayToday(symbolList);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * PER_MINUTE);
            Assert.IsTrue(newStockIntradayHistory[1].Intraday.Count <= symbolList.Count * PER_MINUTE);
            Assert.IsTrue(newStockIntradayHistory[2].Intraday.Count <= symbolList.Count * PER_MINUTE);
            //1 stock, 5 minutes frequency           
            symbolList = new List<string> { "AAPL" };
            timeUnits = 5;
            newStockIntradayHistory = servEnd.GetIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * PER_FIVE_MINUTES);
            //1 stock, 30 minutes frequency           
            symbolList = new List<string> { "GOOG" };
            timeUnits = 30;
            newStockIntradayHistory = servEnd.GetIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * PER_THIRTY_MINUTES);
            //1 stock, 60 minutes frequency           
            symbolList = new List<string> { "INTL" };
            timeUnits = 60;
            newStockIntradayHistory = servEnd.GetIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * PER_SIXTY_MINUTES);
            //1 stock, 1 hour frequency           
            symbolList = new List<string> { "INTL" };
            timeUnits = 1;
            newStockIntradayHistory = servEnd.GetIntradayToday(symbolList, timeUnits, ServiceEndPoints.HOUR_UNIT);
            Assert.IsNotNull(newStockIntradayHistory);
            Assert.IsTrue(newStockIntradayHistory[0].Intraday.Count <= symbolList.Count * PER_HOUR);
            //Parameter error - null list of symbols
            symbolList = null;
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetIntradayToday(symbolList); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - empty list of symbols
            symbolList = new List<string>();
            ex = Assert.Throws<ArgumentException>(delegate { servEnd.GetIntradayToday(symbolList); });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - number of time units
            symbolList = new List<string> { "INTL" };
            timeUnits = -1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newStockIntradayHistory = servEnd.GetIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "numUnits");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (minute)
            symbolList = new List<string> { "AAPL" };
            timeUnits = 1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newStockIntradayHistory = servEnd.GetIntradayToday(symbolList, timeUnits, WRONG_MIN_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (hour)
            symbolList = new List<string> { "AAPL" };
            timeUnits = 1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newStockIntradayHistory = servEnd.GetIntradayToday(symbolList, timeUnits, WRONG_HOUR_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }

        [Test, Description("Service Crypto Daily History calls")]
        public void TestGetCryptoHistory()
        {
            List<CryptoPairHistory> newCryptoHistory = null;
            string symbolList = null;
            int quotes;
            Exception ex;

            //1 crypto pair, 7 quotes
            symbolList = "btcusd";
            quotes = 7;
            newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.AreEqual(newCryptoHistory[0].PriceData.Count, quotes);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes);
            }
            //2 crypto pairs, 10 quotes
            symbolList = "btcusd,ethusd";
            quotes = 10;
            newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.AreEqual(newCryptoHistory[0].PriceData.Count, quotes);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.AreEqual(newCryptoHistory[1].PriceData.Count, quotes);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= quotes);
            }
            //3 crypto pairs, 5 quotes
            symbolList = "ltcusd,btcusd,ethusd";
            quotes = 5;
            newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.AreEqual(newCryptoHistory[0].PriceData.Count, quotes);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.AreEqual(newCryptoHistory[1].PriceData.Count, quotes);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[2].Ticker));
            Assert.AreEqual(newCryptoHistory[2].PriceData.Count, quotes);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= quotes);
            }
            foreach (string exchQuote in newCryptoHistory[2].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[2].ExchangeData[exchQuote].Count <= quotes);
            }
            //3 crypto pairs, 5 quotes, with spaces between symbols
            symbolList = "ltcusd, btcusd, ethusd";
            quotes = 5;
            newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(newCryptoHistory[0].Ticker == null || symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes);
            Assert.IsTrue(newCryptoHistory[1].Ticker == null || symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.IsTrue(newCryptoHistory[1].PriceData.Count <= quotes);
            Assert.IsTrue(newCryptoHistory[2].Ticker == null || symbolList.Contains(newCryptoHistory[2].Ticker));
            Assert.IsTrue(newCryptoHistory[2].PriceData.Count <= quotes);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= quotes);
            }
            foreach (string exchQuote in newCryptoHistory[2].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[2].ExchangeData[exchQuote].Count <= quotes);
            }
            //1 crypto pair, 5 quotes, started with space
            symbolList = " ethusd";
            quotes = 5;
            newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsNull(newCryptoHistory[0].Ticker);
            Assert.AreEqual(newCryptoHistory[0].PriceData.Count, 0);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes);
            }
            //Parameter error - number of quotes
            symbolList = "bchusd";
            quotes = -1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            });
            Assert.AreEqual(ex.Message, "numQuotes");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - null list of symbols
            symbolList = null;
            quotes = 5;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - empty list of symbols
            symbolList = String.Empty;
            quotes = 5;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }

        [Test, Description("Service Crypto Quote calls")]
        public void TestGetCryptoQuote()
        {
            List<CryptoPairQuote> newCryptoQuote = null;
            string symbolList = null;
            Exception ex;

            //1 crypto pair
            symbolList = "btcusd";
            newCryptoQuote = servEnd.GetCryptoQuote(symbolList);
            Assert.IsNotNull(newCryptoQuote);
            Assert.AreEqual(newCryptoQuote.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoQuote[0].Ticker));
            Assert.IsTrue(newCryptoQuote[0].TobData.Count == 1);
            foreach (string exchQuote in newCryptoQuote[0].TobExchanges.Keys)
            {
                Assert.IsTrue(newCryptoQuote[0].TobExchanges[exchQuote].Count <= 1);
            }
            //2 crypto pairs
            symbolList = "btcusd,ethusd";
            newCryptoQuote = servEnd.GetCryptoQuote(symbolList);
            Assert.IsNotNull(newCryptoQuote);
            Assert.AreEqual(newCryptoQuote.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoQuote[0].Ticker));
            Assert.IsTrue(newCryptoQuote[0].TobData.Count == 1);
            Assert.IsTrue(newCryptoQuote[1].TobData.Count == 1);
            foreach (string exchQuote in newCryptoQuote[0].TobExchanges.Keys)
            {
                Assert.IsTrue(newCryptoQuote[0].TobExchanges[exchQuote].Count <= 1);
            }
            foreach (string exchQuote in newCryptoQuote[1].TobExchanges.Keys)
            {
                Assert.IsTrue(newCryptoQuote[1].TobExchanges[exchQuote].Count <= 1);
            }
            //3 crypto pairs
            symbolList = "ltcusd,btcusd,ethusd";
            newCryptoQuote = servEnd.GetCryptoQuote(symbolList);
            Assert.IsNotNull(newCryptoQuote);
            Assert.AreEqual(newCryptoQuote.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoQuote[0].Ticker));
            Assert.IsTrue(newCryptoQuote[0].TobData.Count == 1);
            Assert.IsTrue(newCryptoQuote[1].TobData.Count == 1);
            Assert.IsTrue(newCryptoQuote[2].TobData.Count == 1);
            foreach (string exchQuote in newCryptoQuote[0].TobExchanges.Keys)
            {
                Assert.IsTrue(newCryptoQuote[0].TobExchanges[exchQuote].Count <= 1);
            }
            foreach (string exchQuote in newCryptoQuote[1].TobExchanges.Keys)
            {
                Assert.IsTrue(newCryptoQuote[1].TobExchanges[exchQuote].Count <= 1);
            }
            foreach (string exchQuote in newCryptoQuote[2].TobExchanges.Keys)
            {
                Assert.IsTrue(newCryptoQuote[2].TobExchanges[exchQuote].Count <= 1);
            }
            //3 crypto pairs, with spaces between symbols
            symbolList = "ltcusd, btcusd, ethusd";
            newCryptoQuote = servEnd.GetCryptoQuote(symbolList);
            Assert.IsNotNull(newCryptoQuote);
            Assert.AreEqual(newCryptoQuote.Count, 1);
            Assert.IsTrue(newCryptoQuote[0].Ticker == null || symbolList.Contains(newCryptoQuote[0].Ticker));
            Assert.IsTrue(newCryptoQuote[0].TobData.Count <= 1);
            foreach (string exchQuote in newCryptoQuote[0].TobExchanges.Keys)
            {
                Assert.IsTrue(newCryptoQuote[0].TobExchanges[exchQuote].Count <= 1);
            }
            //1 crypto pair, started with space
            symbolList = " ethusd";
            newCryptoQuote = servEnd.GetCryptoQuote(symbolList);
            Assert.IsNotNull(newCryptoQuote);
            Assert.AreEqual(newCryptoQuote.Count, 0);
            //Parameter error - null list of symbols
            symbolList = null;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoQuote = servEnd.GetCryptoQuote(symbolList);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - empty list of symbols
            symbolList = String.Empty;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoQuote = servEnd.GetCryptoQuote(symbolList);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }

        [Test, Description("Service Crypto Intraday History calls")]
        public void TestGetCryptoIntradayHistory()
        {
            const int PER_MINUTE = 1440;
            const int PER_FIVE_MINUTES = 288;
            const int PER_THIRTY_MINUTES = 48;
            const int PER_SIXTY_MINUTES = 24;
            const int PER_HOUR = 24;

            const string WRONG_MIN_SYMBOL = "m";
            const string WRONG_HOUR_SYMBOL = "h";

            List<CryptoPairHistory> newCryptoHistory = null;
            string symbolList = null;
            int quotes, timeUnits;
            Exception ex;

            //1 crypto pair, 7 quotes
            symbolList = "btcusd";
            quotes = 7;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            //2 crypto pairs, 10 quotes
            symbolList = "btcusd,ethusd";
            quotes = 10;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_MINUTE);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.IsTrue(newCryptoHistory[1].PriceData.Count <= quotes * PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            //3 crypto pairs, 5 quotes
            symbolList = "ltcusd,btcusd,ethusd";
            quotes = 5;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_MINUTE);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.IsTrue(newCryptoHistory[1].PriceData.Count <= quotes * PER_MINUTE);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[2].Ticker));
            Assert.IsTrue(newCryptoHistory[2].PriceData.Count <= quotes * PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[2].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[2].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            //3 crypto pairs, 5 quotes, with spaces between symbols
            symbolList = "ltcusd, btcusd, ethusd";
            quotes = 5;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(newCryptoHistory[0].Ticker == null || symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_MINUTE);
            Assert.IsTrue(newCryptoHistory[1].Ticker == null || symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.IsTrue(newCryptoHistory[1].PriceData.Count <= quotes * PER_MINUTE);
            Assert.IsTrue(newCryptoHistory[2].Ticker == null || symbolList.Contains(newCryptoHistory[2].Ticker));
            Assert.IsTrue(newCryptoHistory[2].PriceData.Count <= quotes * PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[2].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[2].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            //1 crypto pair, 5 quotes, started with space
            symbolList = " ethusd";
            quotes = 5;
            newCryptoHistory = servEnd.GetCryptoHistory(symbolList, quotes * PER_MINUTE);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsNull(newCryptoHistory[0].Ticker);
            Assert.AreEqual(newCryptoHistory[0].PriceData.Count, 0);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_MINUTE);
            }
            //1 crypto pair, 7 quotes, 5 minutes frequency           
            symbolList = "xrpusd";
            quotes = 7;
            timeUnits = 5;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_FIVE_MINUTES);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_FIVE_MINUTES);
            }
            //1 crypto pair, 7 quotes, 30 minutes frequency           
            symbolList = "xtzusd";
            quotes = 7;
            timeUnits = 30;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_THIRTY_MINUTES);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_THIRTY_MINUTES);
            }
            //1 crypto pair, 7 quotes, 60 minutes frequency           
            symbolList = "zecusd";
            quotes = 7;
            timeUnits = 60;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_SIXTY_MINUTES);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_SIXTY_MINUTES);
            }
            //1 crypto pair, 7 quotes, 1 hour frequency           
            symbolList = "zecusd";
            quotes = 7;
            timeUnits = 1;
            newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.HOUR_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= quotes * PER_HOUR);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= quotes * PER_HOUR);
            }
            //Parameter error - number of quotes
            symbolList = "bchusd";
            quotes = -1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes);
            });
            Assert.AreEqual(ex.Message, "numQuotes");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - number of time units
            symbolList = "bchusd";
            quotes = 7;
            timeUnits = -1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "numUnits");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - null list of symbols
            symbolList = null;
            quotes = 7;
            timeUnits = 30;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - empty list of symbols
            symbolList = String.Empty;
            quotes = 7;
            timeUnits = 30;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (minute)
            symbolList = "bchusd";
            quotes = 7;
            timeUnits = 30;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, WRONG_MIN_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (hour)
            symbolList = "bchusd";
            quotes = 7;
            timeUnits = 1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayHistory(symbolList, quotes, timeUnits, WRONG_HOUR_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }

        [Test, Description("Service Crypto Intraday Today calls")]
        public void TestGetCryptoIntradayToday()
        {
            const int PER_MINUTE = 1440;
            const int PER_FIVE_MINUTES = 288;
            const int PER_THIRTY_MINUTES = 48;
            const int PER_SIXTY_MINUTES = 24;
            const int PER_HOUR = 24;

            const string WRONG_MIN_SYMBOL = "m";
            const string WRONG_HOUR_SYMBOL = "h";

            List<CryptoPairHistory> newCryptoHistory = null;
            string symbolList = null;
            int timeUnits;
            Exception ex;

            //1 crypto pair
            symbolList = "btcusd";
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            //2 crypto pairs
            symbolList = "btcusd,ethusd";
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_MINUTE);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.IsTrue(newCryptoHistory[1].PriceData.Count <= PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            //3 crypto pairs
            symbolList = "ltcusd,btcusd,ethusd";
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_MINUTE);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.IsTrue(newCryptoHistory[1].PriceData.Count <= PER_MINUTE);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[2].Ticker));
            Assert.IsTrue(newCryptoHistory[2].PriceData.Count <= PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[2].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[2].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            //3 crypto pairs, with spaces between symbols
            symbolList = "ltcusd, btcusd, ethusd";
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(newCryptoHistory[0].Ticker == null || symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_MINUTE);
            Assert.IsTrue(newCryptoHistory[1].Ticker == null || symbolList.Contains(newCryptoHistory[1].Ticker));
            Assert.IsTrue(newCryptoHistory[1].PriceData.Count <= PER_MINUTE);
            Assert.IsTrue(newCryptoHistory[2].Ticker == null || symbolList.Contains(newCryptoHistory[2].Ticker));
            Assert.IsTrue(newCryptoHistory[2].PriceData.Count <= PER_MINUTE);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[1].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[1].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            foreach (string exchQuote in newCryptoHistory[2].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[2].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            //1 crypto pair, started with space
            symbolList = " ethusd";
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsNull(newCryptoHistory[0].Ticker);
            Assert.AreEqual(newCryptoHistory[0].PriceData.Count, 0);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_MINUTE);
            }
            //1 crypto pair, 5 minutes frequency           
            symbolList = "xrpusd";
            timeUnits = 5;
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_FIVE_MINUTES);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_FIVE_MINUTES);
            }
            //1 crypto pair, 30 minutes frequency           
            symbolList = "xtzusd";
            timeUnits = 30;
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_THIRTY_MINUTES);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_THIRTY_MINUTES);
            }
            //1 crypto pair, 60 minutes frequency           
            symbolList = "zecbtc";
            timeUnits = 60;
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_SIXTY_MINUTES);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_SIXTY_MINUTES);
            }
            //1 crypto pair, 1 hour frequency           
            symbolList = "zecbtc";
            timeUnits = 1;
            newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, ServiceEndPoints.HOUR_UNIT);
            Assert.IsNotNull(newCryptoHistory);
            Assert.AreEqual(newCryptoHistory.Count, symbolList.Split(',').Length);
            Assert.IsTrue(symbolList.Contains(newCryptoHistory[0].Ticker));
            Assert.IsTrue(newCryptoHistory[0].PriceData.Count <= PER_HOUR);
            foreach (string exchQuote in newCryptoHistory[0].ExchangeData.Keys)
            {
                Assert.IsTrue(newCryptoHistory[0].ExchangeData[exchQuote].Count <= PER_HOUR);
            }
            //Parameter error - number of time units
            symbolList = "bchusd";
            timeUnits = -1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "numUnits");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - null list of symbols
            symbolList = null;
            timeUnits = 30;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - empty list of symbols
            symbolList = String.Empty;
            timeUnits = 30;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, ServiceEndPoints.MINUTE_UNIT);
            });
            Assert.AreEqual(ex.Message, "symbols");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (minute)
            symbolList = "bchusd";
            timeUnits = 30;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, WRONG_MIN_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
            //Parameter error - wrong time unit (hour)
            symbolList = "bchusd";
            timeUnits = 1;
            ex = Assert.Throws<ArgumentException>(delegate
            {
                newCryptoHistory = servEnd.GetCryptoIntradayToday(symbolList, timeUnits, WRONG_HOUR_SYMBOL);
            });
            Assert.AreEqual(ex.Message, "units");
            Assert.IsInstanceOf(typeof(ArgumentException), ex);
        }
    }
}