﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Helper.APIManager
{
    public class APISelector
    {
        private static List<string> _APIList = new List<string>() { "c30f0dd066ea2c3f7d1c2d97e1db87bfbb4c11c7" };
        private static List<int> weights = new List<int>();
        private static APISelector instance = null;
        private static Random randomGenerator = new Random();
        private static int totalWeights;
        private static Mutex weightMutex = new Mutex();
        private static Timer schedule = null;
        private static int limit;

        public static void setTiming()
        {
            DateTime current = DateTime.Now;
            DateTime next = current.AddHours(1);
            next = new DateTime(next.Year, next.Month, next.Day, next.Hour, 0, 0);
            TimeSpan timeToInit = next - current;

            if (timeToInit < TimeSpan.Zero)
            {
                return;
            }

            schedule = new Timer(x =>
            {
                Instance = null;
                new APISelector(Limit);
            }, null, timeToInit, Timeout.InfiniteTimeSpan);
        }

        public APISelector(int limit)
        {
            WeightMutex.WaitOne();
            if (Instance == null)
            {
                Instance = this;
                TotalWeights = limit;
                Limit = limit;
                for (int i = 0; i < APIList.Count; i++)
                {
                    if (i != APIList.Count - 1)
                        weights.Add((int)Math.Round((decimal)((i + 1) * (TotalWeights / APIList.Count)), 0, MidpointRounding.ToPositiveInfinity));
                    else
                        weights.Add(limit);
                }
            }
            WeightMutex.ReleaseMutex();
            setTiming();
        }

        public string getAPI()
        {
            int sumweights = 0;
            int i = -1;

            int weight = RandomGenerator.Next(1, TotalWeights);

            WeightMutex.WaitOne();
            while (sumweights < weight && i < APIList.Count)
            {
                sumweights += APISelector.Weights[++i];
            }

            TotalWeights--;
            Weights[i]--;
            WeightMutex.ReleaseMutex();

            return APIList[i];
        }

        public static List<string> APIList { get => _APIList; set => _APIList = value; }
        public static List<int> Weights { get => weights; set => weights = value; }
        public static APISelector Instance { get => instance; set => instance = value; }
        public static Random RandomGenerator { get => randomGenerator; set => randomGenerator = value; }
        public static int TotalWeights { get => totalWeights; set => totalWeights = value; }
        public static Mutex WeightMutex { get => weightMutex; set => weightMutex = value; }
        public static Timer Schedule { get => schedule; set => schedule = value; }
        public static int Limit { get => limit; set => limit = value; }
    }
}
