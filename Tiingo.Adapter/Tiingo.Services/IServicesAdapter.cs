﻿using System.Collections.Generic;
using Tiingo.Results;

namespace Tiingo.Services
{
    public interface IServicesAdapter
    {
        public List<HistoryReturn> GetHistory(List<string> symbols, int numQuotes);
        public List<StockQuote> GetQuote(string symbols);
        public List<IntradayHistory> GetIntradayHistory(List<string> symbols, int numQuotes);
        public List<IntradayHistory> GetIntradayHistory(List<string> symbols, int numQuotes, int? numUnits, string? units);
        public List<IntradayHistory> GetIntradayToday(List<string> symbols);
        public List<IntradayHistory> GetIntradayToday(List<string> symbols, int? numUnits, string? units);

    }
}
