﻿using System.Collections.Generic;
using Tiingo.Cryptocurrency;

namespace Tiingo.Services
{
    public interface ICryptoAdapter
    {
        public List<CryptoPairHistory> GetCryptoHistory(string symbols, int numQuotes);
        public List<CryptoPairQuote> GetCryptoQuote(string symbols);
        public List<CryptoPairHistory> GetCryptoIntradayHistory(string symbols, int numQuotes);
        public List<CryptoPairHistory> GetCryptoIntradayHistory(string symbols, int numQuotes, int? numUnits, string? units);
        public List<CryptoPairHistory> GetCryptoIntradayToday(string symbols);
        public List<CryptoPairHistory> GetCryptoIntradayToday(string symbols, int? numUnits, string? units);
    }
}
