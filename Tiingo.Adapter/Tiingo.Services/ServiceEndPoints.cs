﻿using Helper.APIManager;
using Helper.DateTimeExtension;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Tiingo.Cryptocurrency;
using Tiingo.Results;
using Tiingo.WebCode;

namespace Tiingo.Services
{
    public class ServiceEndPoints : IServicesAdapter, ICryptoAdapter
    {
        private const string TIINGO_STOCK_HISTORY_API = "https://api.tiingo.com/tiingo/daily/{0}/prices?startDate={1}&endDate={2}&resampleFreq={3}";
        private const string TIINGO_STOCK_METADATA_API = "https://api.tiingo.com/tiingo/daily/{0}";
        private const string TIINGO_STOCK_QUOTE_API = "https://api.tiingo.com/iex/{0}";
        private const string TIINGO_STOCK_INTRADAY_HIST_API = "https://api.tiingo.com/iex/{0}/prices?startDate={1}&endDate={2}&resampleFreq={3}";
        private const string TIINGO_TODAY_INTRADAY_API = "https://api.tiingo.com/iex/{0}/prices?startDate={1}&endDate={2}&resampleFreq={3}";

        private const string TIINGO_CRYPTO_HISTORY_API = "https://api.tiingo.com/tiingo/crypto/prices?tickers={0}&startDate={1}&endDate={2}&includeRawExchangeData=true&resampleFreq={3}";
        private const string TIINGO_CRYPTO_METADATA_API = "https://api.tiingo.com/tiingo/crypto?tickers={0}";
        private const string TIINGO_CRYPTO_QUOTE_API = "https://api.tiingo.com/tiingo/crypto/top?tickers={0}&includeRawExchangeData=true";
        private const string TTINGO_CRYPTO_ENDPOINT = "crypto";

        private const string AUTH_MODE = "Token ";
        private const string CONTENT = "Content-Type";
        private const string AUTHORIZATION = "Authorization";

        private const string JSON_MEDIA_TYPE = "application/json";
        private const string EMPTY_JSON_ARRAY = "[]";
        private const string EMPTY_JSON_OBJECT = "{}";

        private const string TICKER_NOT_FOUND = "Ticker {0} not found by API. Review if any spaces were included between or before the tickers.";

        public const string MINUTE_UNIT = "min";
        public const string HOUR_UNIT = "hour";
        public const string DAY_UNIT = "day";
        public const string DAILY_FREQ = "daily";

        public const int HOUR_REQUESTS = 500;
        private static APISelector selector = new APISelector(HOUR_REQUESTS);

        private static ServiceEndPoints instance = null;
        private static IDictionary<int, IDictionary<DateTime, string>> holidayFullList = null;
        private static IEnumerable<DateTime> yearHolidays = null;
        private static Mutex holidayMutex = new Mutex();

        private static bool isCryptoURL(string URL)
        {
            return URL.Contains(TTINGO_CRYPTO_ENDPOINT);
        }

        private static string ToJSONArray(string JSONstring)
        {
            char startArray = EMPTY_JSON_ARRAY[0];
            char endArray = EMPTY_JSON_ARRAY[EMPTY_JSON_ARRAY.Length - 1];
            string startJsonArray, JsonResult, endJsonArray;

            if (JSONstring == null)
                return JSONstring;

            if (JSONstring.StartsWith(startArray))
                startJsonArray = string.Empty;
            else
                startJsonArray = startArray.ToString();

            if (JSONstring.EndsWith(endArray))
                endJsonArray = string.Empty;
            else
                endJsonArray = endArray.ToString();

            JsonResult = startJsonArray + JSONstring + endJsonArray;

            return JsonResult;
        }

        private static string ToJSONObject(string JSONstring)
        {
            char startArray = EMPTY_JSON_ARRAY[0];
            char endArray = EMPTY_JSON_ARRAY[EMPTY_JSON_ARRAY.Length - 1];
            string startJsonArray, JsonResult, endJsonArray;

            if (JSONstring == null)
                return JSONstring;

            if (JSONstring.StartsWith(startArray))
                JsonResult = JSONstring.Remove(0, 1);
            else
                JsonResult = JSONstring;

            if (JsonResult.EndsWith(endArray))
                JsonResult = JsonResult.Remove(JsonResult.Length - 1);

            return JsonResult;
        }

        private static async Task<string> StreamToStringAsync(Stream stream)
        {
            string content = null;

            if (stream != null)
                using (StreamReader sr = new StreamReader(stream))
                    content = await sr.ReadToEndAsync();

            return content;
        }

        private static async Task<WebStatus> ReadContent(HttpRequestMessage request, HttpCompletionOption opt, CancellationToken ct)
        {
            string API_KEY = selector.getAPI();
            HttpClient httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(JSON_MEDIA_TYPE));
            httpClient.DefaultRequestHeaders.Add(AUTHORIZATION, AUTH_MODE + API_KEY);

            HttpResponseMessage response = await httpClient.SendAsync(request, opt, ct);
            Stream stream = await response.Content.ReadAsStreamAsync();
            HttpStatusCode responseCode = response.StatusCode;
            string httpContent = await StreamToStringAsync(stream);

            return new WebStatus(responseCode, httpContent);
        }

        private static T generateObjectReturn<T>(SortedDictionary<string, Task<WebStatus>> content,
                                                 SortedDictionary<string, Task<WebStatus>> metaContent,
                                                 string ticker)
        {
            string jsonHistoryContent = EMPTY_JSON_ARRAY;
            string jsonMetaContent = EMPTY_JSON_OBJECT;
            string jsonMessage = null;
            HttpStatusCode code = HttpStatusCode.OK;

            string tempMessage = null;

            if (content.ContainsKey(ticker) && metaContent.ContainsKey(ticker))
            {
                if (content[ticker].Result.Code == (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetTypeCode()) &&
                    metaContent[ticker].Result.Code == (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetTypeCode()))
                {
                    jsonHistoryContent = ToJSONArray(content[ticker].Result.Message);
                    jsonMetaContent = metaContent[ticker].Result.Message;
                    jsonMessage = WebStatus.MSG_HTTPOK;
                    code = Enum.Parse<HttpStatusCode>(content[ticker].Result.Code.ToString());
                }
                else
                {
                    if (content[ticker].Result.Code != (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetTypeCode()))
                    {
                        jsonMessage = content[ticker].Result.Message;
                        code = Enum.Parse<HttpStatusCode>(content[ticker].Result.Code.ToString());
                        jsonHistoryContent = EMPTY_JSON_ARRAY;
                    }
                    else if (metaContent[ticker].Result.Code != (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetTypeCode()))
                    {
                        jsonMessage = metaContent[ticker].Result.Message;
                        code = Enum.Parse<HttpStatusCode>(metaContent[ticker].Result.Code.ToString());
                        jsonMetaContent = EMPTY_JSON_OBJECT;
                    }
                }

                try
                {
                    tempMessage = (string)JObject.Parse(jsonMessage).SelectToken("detail");
                }
                catch (JsonReaderException JRExcp)
                {

                }
                catch (ArgumentNullException ANExcp)
                {

                }
                finally
                {
                    if (tempMessage != null)
                    {
                        if (code == HttpStatusCode.OK)
                        {
                            code = HttpStatusCode.BadRequest;
                        }
                        jsonMessage = tempMessage;
                        jsonHistoryContent = EMPTY_JSON_ARRAY;
                    }
                }
            }

            if (!content.ContainsKey(ticker))
            {
                jsonHistoryContent = EMPTY_JSON_ARRAY;
                if (code == HttpStatusCode.OK)
                {
                    code = HttpStatusCode.BadRequest;
                    jsonMessage = String.Format(TICKER_NOT_FOUND, ticker);
                }
            }

            if (!metaContent.ContainsKey(ticker))
            {
                jsonMetaContent = EMPTY_JSON_OBJECT;
                if (code == HttpStatusCode.OK)
                {
                    code = HttpStatusCode.BadRequest;
                    jsonMessage = String.Format(TICKER_NOT_FOUND, ticker);
                }
            }

            JObject testParse = JObject.Parse(jsonMetaContent);
            JArray testArrayParse = JArray.Parse(jsonHistoryContent);
            T newInstance = (T)Activator.CreateInstance(typeof(T),
                                                         new Object[] {
                                                                        code,
                                                                        jsonMessage,
                                                                        JObject.Parse(jsonMetaContent),
                                                                        JArray.Parse(jsonHistoryContent)
                                                                      });

            return newInstance;
        }

        private static async Task<List<TReturn>> GetAsyncTiingoResult<TReturn>(List<string> symbols,
                                                                               string returnURL,
                                                                               CancellationToken cancellationToken)
        {
            Mutex lockMeta = new Mutex();
            Mutex lockHistory = new Mutex();

            List<Task<HttpResponseMessage>> historyResponse = new List<Task<HttpResponseMessage>>();
            List<Task<HttpResponseMessage>> metaResponse = new List<Task<HttpResponseMessage>>();

            SortedDictionary<string, Task<WebStatus>> historyContent = new SortedDictionary<string, Task<WebStatus>>();
            SortedDictionary<string, Task<WebStatus>> metaContent = new SortedDictionary<string, Task<WebStatus>>();

            List<Task> allTasks = new List<Task>();
            List<Task> continuationTasks = new List<Task>();
            List<string> symbolList;

            Mutex lockReturn = new Mutex();
            List<TReturn> allReturns = new List<TReturn>();

            string historyUrl, metaUrl;
            Task h;
            Task control;
            DateTime today = DateTime.Today;

            if (!isCryptoURL(returnURL))
                symbolList = symbols;
            else
                symbolList = symbols[0].Split(',').ToList<string>();

            TaskFactory taskFactory = new TaskFactory(cancellationToken,
                                                      TaskCreationOptions.AttachedToParent,
                                                      TaskContinuationOptions.ExecuteSynchronously,
                                                      null);

            for (int i = 0; i < symbols.Distinct().Count(); i++)
            {
                if (!isCryptoURL(returnURL))
                {
                    historyUrl = String.Format(returnURL, symbols[i]);
                    metaUrl = String.Format(TIINGO_STOCK_METADATA_API, symbols[i]);
                }
                else
                {
                    historyUrl = String.Format(returnURL, symbols[i]);
                    metaUrl = String.Format(TIINGO_CRYPTO_METADATA_API, symbols[i]);
                }

                Task<WebStatus> tw1 = taskFactory.StartNew((ticker) =>
                {
                    List<Task> contentTasks = new List<Task>();
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, metaUrl);
                    Task<WebStatus> taskContent = ReadContent(request,
                                                              HttpCompletionOption.ResponseHeadersRead,
                                                              cancellationToken);

                    taskContent.Wait();

                    if (!isCryptoURL(returnURL))
                    {
                        lockMeta.WaitOne();
                        metaContent.Add((string)ticker, taskContent);
                        lockMeta.ReleaseMutex();
                    }
                    else
                    {
                        JArray contentArray = JArray.Parse(taskContent.Result.Message);
                        foreach (JToken content in contentArray)
                        {
                            Task<WebStatus> task = taskFactory.StartNew((content) =>
                            {
                                return new WebStatus(taskContent.Result.Code, ((JToken)content).ToString());
                            }, content, cancellationToken);

                            contentTasks.Add(task);
                        }

                        Task holdContent = Task.WhenAll(contentTasks);
                        holdContent.Wait();

                        foreach (Task<WebStatus> taskWeb in contentTasks)
                        {
                            JToken webContent = JObject.Parse(taskWeb.Result.Message);
                            if (webContent.SelectToken("ticker") != null &&
                                (string)webContent.SelectToken("ticker") != string.Empty)
                            {
                                lockMeta.WaitOne();
                                metaContent.Add((string)webContent.SelectToken("ticker"), taskWeb);
                                lockMeta.ReleaseMutex();
                            }

                        }
                    }

                    return taskContent.Result;
                }, symbols[i], cancellationToken);

                Task<WebStatus> tw2 = taskFactory.StartNew((ticker) =>
                {
                    List<Task> contentTasks = new List<Task>();
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, historyUrl);

                    Task<WebStatus> taskContent = ReadContent(request,
                                                              HttpCompletionOption.ResponseHeadersRead,
                                                              cancellationToken);

                    taskContent.Wait();

                    if (!isCryptoURL(returnURL))
                    {
                        lockHistory.WaitOne();
                        historyContent.Add((string)ticker, taskContent);
                        lockHistory.ReleaseMutex();
                    }
                    else
                    {
                        JArray contentArray = JArray.Parse(taskContent.Result.Message);
                        foreach (JToken content in contentArray)
                        {
                            Task<WebStatus> task = taskFactory.StartNew((content) =>
                            {
                                return new WebStatus(taskContent.Result.Code, ((JToken)content).ToString());
                            }, content, cancellationToken);

                            contentTasks.Add(task);
                        }

                        Task holdContent = Task.WhenAll(contentTasks);
                        holdContent.Wait();

                        foreach (Task<WebStatus> taskWeb in contentTasks)
                        {
                            JToken webContent = JObject.Parse(taskWeb.Result.Message);
                            if (webContent.SelectToken("ticker") != null &&
                                (string)webContent.SelectToken("ticker") != string.Empty)
                            {
                                lockHistory.WaitOne();
                                historyContent.Add((string)webContent.SelectToken("ticker"), taskWeb);
                                lockHistory.ReleaseMutex();
                            }
                        }
                    }

                    return taskContent.Result;
                }, symbols[i], cancellationToken);


                allTasks.Add(tw1);
                allTasks.Add(tw2);
            }

            control = Task.WhenAll(allTasks);
            await control;

            foreach (string ticker in symbolList.Distinct())
            {
                h = taskFactory.StartNew((symbol) =>
                {
                    string ticker = (string)symbol;

                    if (ticker != null && ticker != string.Empty)
                    {
                        lockReturn.WaitOne();
                        allReturns.Add(generateObjectReturn<TReturn>(historyContent,
                                                                     metaContent,
                                                                     ticker));
                        lockReturn.ReleaseMutex();
                    }
                }, ticker, cancellationToken);

                continuationTasks.Add(h);
            }
            control = Task.WhenAll(continuationTasks);
            await control;

            return allReturns;
        }

        private static async Task<List<TReturn>> GetAsyncTiingoQuote<TReturn>(string symbols,
                                                                              string returnURL,
                                                                              CancellationToken cancellationToken)
        {
            string priceUrl;
            HttpClient client;
            TaskFactory taskFactory;
            Task<List<TReturn>> taskQuote;

            List<string> symbolList;
            string message, content, tempMessage = null;

            priceUrl = String.Format(returnURL, symbols);

            taskFactory = new TaskFactory(cancellationToken,
                                          TaskCreationOptions.AttachedToParent,
                                          TaskContinuationOptions.ExecuteSynchronously,
                                          null);
            taskQuote = taskFactory.StartNew((url) =>
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, (string)url);
                Task<WebStatus> taskContent = ReadContent(request,
                                                          HttpCompletionOption.ResponseHeadersRead,
                                                          cancellationToken);
                taskContent.Wait();

                if (taskContent.Result.Code == (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetTypeCode()))
                {
                    message = WebStatus.MSG_HTTPOK;
                    content = taskContent.Result.Message;
                }
                else
                {
                    message = taskContent.Result.Message;
                    content = EMPTY_JSON_ARRAY;
                }

                try
                {
                    tempMessage = (string)JObject.Parse(message).SelectToken("detail");
                }
                catch (JsonReaderException JRExcp)
                {

                }
                catch (ArgumentNullException ANExcp)
                {

                }
                finally
                {
                    if (tempMessage != null)
                    {
                        if (taskContent.Result.Code == (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetTypeCode()))
                        {
                            taskContent.Result.Code = (int)Convert.ChangeType(HttpStatusCode.BadRequest, HttpStatusCode.BadRequest.GetTypeCode());
                        }
                        message = tempMessage;
                        content = EMPTY_JSON_ARRAY;
                    }
                }

                List<TReturn> returnList = new List<TReturn>();
                TReturn newInstance;
                string newJsonArray;

                if (!isCryptoURL(returnURL))
                {
                    newInstance = (TReturn)Activator.CreateInstance(typeof(TReturn),
                                                                    new Object[]
                                                                    {
                                                                         Enum.Parse<HttpStatusCode>(taskContent.Result.Code.ToString()),
                                                                         message,
                                                                         JArray.Parse(content)
                                                                    });
                    returnList.Add(newInstance);
                }
                else
                {
                    foreach (JToken token in JArray.Parse(content))
                    {
                        newJsonArray = ToJSONArray(token.ToString());
                        newInstance = (TReturn)Activator.CreateInstance(typeof(TReturn),
                                                                        new Object[]
                                                                        {
                                                                             Enum.Parse<HttpStatusCode>(taskContent.Result.Code.ToString()),
                                                                             message,
                                                                             JArray.Parse(newJsonArray)
                                                                        });
                        returnList.Add(newInstance);
                    }
                }

                return returnList;
            }
            , priceUrl, cancellationToken);
            await taskQuote;

            return taskQuote.Result;
        }

        public static ServiceEndPoints getServiceEndPoints()
        {
            int actualYear = DateTime.Today.Year;

            HolidayMutex.WaitOne();
            if (HolidayFullList == null)
                HolidayFullList = DateTimeExtensions.LoadHolidays();
            if (YearHolidays == null)
                YearHolidays = DateTimeExtensions.GetHolidays(new List<int> { actualYear - 1, actualYear, actualYear + 1 }.AsEnumerable(),
                                                              HolidayFullList);
            HolidayMutex.ReleaseMutex();

            if (instance == null)
                instance = new ServiceEndPoints();

            return instance;
        }

        public ServiceEndPoints()
        {

        }

        public List<HistoryReturn> GetHistory(List<string> symbols, int numQuotes)
        {
            List<HistoryReturn> results = null;
            List<Task> TaskList = new List<Task>();
            DateTime today = DateTime.Today;

            if (numQuotes < 1) throw new ArgumentException(nameof(numQuotes));
            if (symbols == null || symbols.Count < 1) throw new ArgumentException(nameof(symbols));

            string historyUrl = String.Format(TIINGO_STOCK_HISTORY_API, "{0}",
                                                                        DateTimeExtensions.SubtractBusinessDays(today,
                                                                                                                numQuotes,
                                                                                                                YearHolidays).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                        DateTimeExtensions.SubtractBusinessDays(today,
                                                                                                                1,
                                                                                                                YearHolidays).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                        DAILY_FREQ);

            GetAsyncTiingoResult<HistoryReturn>(symbols,
                                                historyUrl,
                                                CancellationToken.None).Wait();
            results = GetAsyncTiingoResult<HistoryReturn>(symbols,
                                                          historyUrl,
                                                          CancellationToken.None).Result;

            return results;
        }

        public List<StockQuote> GetQuote(string symbols)
        {
            if (symbols == null || symbols.Length < 1) throw new ArgumentException(nameof(symbols));

            GetAsyncTiingoQuote<StockQuote>(symbols, TIINGO_STOCK_QUOTE_API, CancellationToken.None).Wait();
            List<StockQuote> quote = GetAsyncTiingoQuote<StockQuote>(symbols, TIINGO_STOCK_QUOTE_API, CancellationToken.None).Result;
            return quote;
        }

        public List<IntradayHistory> GetIntradayHistory(List<string> symbols, int numQuotes, int? numUnits, string? units)
        {
            List<IntradayHistory> intradayHistories = null;
            DateTime today = DateTime.Today;

            if (numQuotes < 1) throw new ArgumentException(nameof(numQuotes));
            if (symbols == null || symbols.Count < 1) throw new ArgumentException(nameof(symbols));
            if (numUnits != null && numUnits <= 0) throw new ArgumentException(nameof(numUnits));
            if (units != null && units != MINUTE_UNIT && units != HOUR_UNIT) throw new ArgumentException(nameof(units));

            string historyUrl = String.Format(TIINGO_STOCK_INTRADAY_HIST_API, "{0}",
                                                                              DateTimeExtensions.SubtractBusinessDays(today,
                                                                                                                      numQuotes,
                                                                                                                      YearHolidays).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                              DateTimeExtensions.SubtractBusinessDays(today,
                                                                                                                      1,
                                                                                                                      YearHolidays).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                              (numUnits ?? 1).ToString() + (units ?? MINUTE_UNIT));

            GetAsyncTiingoResult<IntradayHistory>(symbols,
                                                  historyUrl,
                                                  CancellationToken.None).Wait();
            intradayHistories = GetAsyncTiingoResult<IntradayHistory>(symbols,
                                                                      historyUrl,
                                                                      CancellationToken.None).Result;

            return intradayHistories;
        }

        public List<IntradayHistory> GetIntradayHistory(List<string> symbols, int numQuotes)
        {
            return GetIntradayHistory(symbols, numQuotes, null, null);
        }

        public List<IntradayHistory> GetIntradayToday(List<string> symbols, int? numUnits, string? units)
        {
            List<IntradayHistory> intradayHistories = null;
            DateTime today = DateTime.Today;

            if (symbols == null || symbols.Count < 1) throw new ArgumentException(nameof(symbols));
            if (numUnits != null && numUnits <= 0) throw new ArgumentException(nameof(numUnits));
            if (units != null && units != MINUTE_UNIT && units != HOUR_UNIT) throw new ArgumentException(nameof(units));

            string historyUrl = String.Format(TIINGO_STOCK_INTRADAY_HIST_API, "{0}",
                                                                              today.ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                              today.ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                              (numUnits ?? 1).ToString() + (units ?? MINUTE_UNIT));

            GetAsyncTiingoResult<IntradayHistory>(symbols,
                                                  historyUrl,
                                                  CancellationToken.None).Wait();
            intradayHistories = GetAsyncTiingoResult<IntradayHistory>(symbols,
                                                                      historyUrl,
                                                                      CancellationToken.None).Result;

            IEnumerable<IntradayHistory> selectedIntraday = intradayHistories.Where((idHist) =>
                                                                                    (idHist.Status.Code == (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetType()) &&
                                                                                     idHist.Intraday.Count < 1));
            if (intradayHistories == null ||
                intradayHistories.Count < 1 ||
                selectedIntraday.Count() > 0)
            {
                historyUrl = String.Format(TIINGO_STOCK_INTRADAY_HIST_API, "{0}",
                                                                           DateTimeExtensions.SubtractBusinessDays(today,
                                                                                                                   1,
                                                                                                                   YearHolidays).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                           DateTimeExtensions.SubtractBusinessDays(today,
                                                                                                                   1,
                                                                                                                   YearHolidays).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                           (numUnits ?? 1).ToString() + (units ?? MINUTE_UNIT));

                GetAsyncTiingoResult<IntradayHistory>(symbols,
                                                      historyUrl,
                                                      CancellationToken.None).Wait();
                intradayHistories = GetAsyncTiingoResult<IntradayHistory>(symbols,
                                                                          historyUrl,
                                                                          CancellationToken.None).Result;
            }

            return intradayHistories;
        }

        public List<IntradayHistory> GetIntradayToday(List<string> symbols)
        {
            return GetIntradayToday(symbols, null, null);
        }

        public List<CryptoPairQuote> GetCryptoQuote(string symbols)
        {
            if (symbols == null || symbols.Length < 1) throw new ArgumentException(nameof(symbols));

            GetAsyncTiingoQuote<CryptoPairQuote>(symbols, TIINGO_CRYPTO_QUOTE_API, CancellationToken.None).Wait();
            List<CryptoPairQuote> quote = GetAsyncTiingoQuote<CryptoPairQuote>(symbols, TIINGO_CRYPTO_QUOTE_API, CancellationToken.None).Result;
            return quote;
        }

        public List<CryptoPairHistory> GetCryptoIntradayHistory(string symbols, int numQuotes, int? numUnits, string? units)
        {
            List<CryptoPairHistory> cryptoHistories = null;

            if (numQuotes < 1) throw new ArgumentException(nameof(numQuotes));
            if (symbols == null || symbols.Length < 1) throw new ArgumentException(nameof(symbols));
            if (numUnits != null && numUnits <= 0) throw new ArgumentException(nameof(numUnits));
            if (units != null && units != MINUTE_UNIT && units != HOUR_UNIT && units != DAY_UNIT)
                throw new ArgumentException(nameof(units));

            DateTime today = DateTime.Today;
            string historyUrl = String.Format(TIINGO_CRYPTO_HISTORY_API, "{0}",
                                                                         today.AddDays(-numQuotes).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                         today.AddDays(-1).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                         (numUnits ?? 1).ToString() + (units ?? MINUTE_UNIT));

            GetAsyncTiingoResult<CryptoPairHistory>(new List<string>() { symbols },
                                                    historyUrl,
                                                    CancellationToken.None).Wait();
            cryptoHistories = GetAsyncTiingoResult<CryptoPairHistory>(new List<string>() { symbols },
                                                                      historyUrl,
                                                                      CancellationToken.None).Result;

            return cryptoHistories;
        }

        public List<CryptoPairHistory> GetCryptoHistory(string symbols, int numQuotes)
        {
            return GetCryptoIntradayHistory(symbols, numQuotes, 1, DAY_UNIT);
        }

        public List<CryptoPairHistory> GetCryptoIntradayHistory(string symbols, int numQuotes)
        {
            return GetCryptoIntradayHistory(symbols, numQuotes, 1, MINUTE_UNIT);
        }

        public List<CryptoPairHistory> GetCryptoIntradayToday(string symbols, int? numUnits, string? units)
        {
            List<CryptoPairHistory> cryptoHistories = null;

            if (symbols == null || symbols.Length < 1) throw new ArgumentException(nameof(symbols));
            if (numUnits != null && numUnits <= 0) throw new ArgumentException(nameof(numUnits));
            if (units != null && units != MINUTE_UNIT && units != HOUR_UNIT && units != DAY_UNIT)
                throw new ArgumentException(nameof(units));

            DateTime today = DateTime.Today;
            string historyUrl = String.Format(TIINGO_CRYPTO_HISTORY_API, "{0}",
                                                                         today.ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                         today.ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                         (numUnits ?? 1).ToString() + (units ?? MINUTE_UNIT));

            GetAsyncTiingoResult<CryptoPairHistory>(new List<string>() { symbols },
                                                    historyUrl,
                                                    CancellationToken.None).Wait();
            cryptoHistories = GetAsyncTiingoResult<CryptoPairHistory>(new List<string>() { symbols },
                                                                      historyUrl,
                                                                      CancellationToken.None).Result;

            IEnumerable<CryptoPairHistory> selectedIntraday = cryptoHistories.Where((idHist) =>
                                                                                    (idHist.Status.Code == (int)Convert.ChangeType(HttpStatusCode.OK, HttpStatusCode.OK.GetType()) &&
                                                                                     idHist.PriceData.Count < 1));
            if (cryptoHistories == null ||
                cryptoHistories.Count < 1 ||
                selectedIntraday.Count() > 0)
            {
                historyUrl = String.Format(TIINGO_STOCK_INTRADAY_HIST_API, "{0}",
                                                                           today.AddDays(-1).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                           today.AddDays(-1).ToString(ConversionHelper.SHORTDATE_FORMAT),
                                                                           (numUnits ?? 1).ToString() + (units ?? MINUTE_UNIT));

                GetAsyncTiingoResult<CryptoPairHistory>(new List<string>() { symbols },
                                                      historyUrl,
                                                      CancellationToken.None).Wait();
                cryptoHistories = GetAsyncTiingoResult<CryptoPairHistory>(new List<string>() { symbols },
                                                                          historyUrl,
                                                                          CancellationToken.None).Result;
            }

            return cryptoHistories;
        }

        public List<CryptoPairHistory> GetCryptoIntradayToday(string symbols)
        {
            return GetCryptoIntradayToday(symbols, null, null);
        }

        public static ServiceEndPoints Instance { get => instance; set => instance = value; }
        public static IDictionary<int, IDictionary<DateTime, string>> HolidayFullList { get => holidayFullList; set => holidayFullList = value; }
        public static IEnumerable<DateTime> YearHolidays { get => yearHolidays; set => yearHolidays = value; }
        public static Mutex HolidayMutex { get => holidayMutex; set => holidayMutex = value; }
    }
}
