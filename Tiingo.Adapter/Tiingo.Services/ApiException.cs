﻿using System;

namespace Tiingo.Services
{
    public class ApiException : Exception
    {
        private int statusCode;
        private string content;
        public int StatusCode { get => statusCode; set => statusCode = value; }
        public string Content { get => content; set => content = value; }
    }
}
