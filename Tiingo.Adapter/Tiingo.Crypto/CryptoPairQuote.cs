﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using Tiingo.WebCode;

namespace Tiingo.Cryptocurrency
{
    public class CryptoPairQuote
    {
        private WebStatus status;
        private string? ticker;
        private string? baseCurrency;
        private string? quoteCurrency;
        private List<ExchangeQuote> tobData = new List<ExchangeQuote>();
        private SortedDictionary<string, List<ExchangeQuote>> tobExchanges = new SortedDictionary<string, List<ExchangeQuote>>();

        public CryptoPairQuote()
        {
        }

        public CryptoPairQuote(WebStatus status,
                               string? ticker,
                               string? baseCurrency,
                               string? quoteCurrency,
                               List<ExchangeQuote> tobData,
                               SortedDictionary<string, List<ExchangeQuote>> tobExchanges)
        {
            Status = status;
            Ticker = ticker;
            BaseCurrency = baseCurrency;
            QuoteCurrency = quoteCurrency;
            TobData = tobData;
            TobExchanges = tobExchanges;
        }

        public CryptoPairQuote(HttpStatusCode code,
                               string message,
                               JArray tobToken)
        {
            JToken? tobDataToken = (JToken?)(tobToken != null && tobToken.Count > 0 ? tobToken[0] : (JToken)null);

            Status = new WebStatus(code, (message ?? WebStatus.MSG_HTTPOK));
            if (tobDataToken != null)
            {
                Ticker = (string)tobDataToken.SelectToken("ticker");
                BaseCurrency = (string)tobDataToken.SelectToken("baseCurrency");
                QuoteCurrency = (string)tobDataToken.SelectToken("quoteCurrency");
                JArray tob = (JArray)tobDataToken.SelectToken("topOfBookData");
                foreach (JToken tobd in tob)
                {
                    TobData.Add(new ExchangeQuote(tobd));
                }
                JToken tobExchData = tobDataToken.SelectToken("exchangeData");
                foreach (JToken tobExch in tobExchData)
                {
                    string exchange = tobExch.Path.Substring(tobExch.Path.LastIndexOf('.') + 1);
                    List<ExchangeQuote> exchangeQuote = new List<ExchangeQuote>();
                    foreach (JToken exchQuote in tobExch.Values())
                    {
                        exchangeQuote.Add(new ExchangeQuote(exchQuote));
                    }
                    TobExchanges.Add(exchange, exchangeQuote);
                }
            }
        }

        public WebStatus Status { get => status; set => status = value; }
        public string? Ticker { get => ticker; set => ticker = value; }
        public string? BaseCurrency { get => baseCurrency; set => baseCurrency = value; }
        public string? QuoteCurrency { get => quoteCurrency; set => quoteCurrency = value; }
        public List<ExchangeQuote> TobData { get => tobData; set => tobData = value; }
        public SortedDictionary<string, List<ExchangeQuote>> TobExchanges { get => tobExchanges; set => tobExchanges = value; }
    }
}
