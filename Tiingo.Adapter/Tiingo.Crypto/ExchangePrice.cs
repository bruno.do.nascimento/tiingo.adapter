﻿using Newtonsoft.Json.Linq;
using System;
using System.Globalization;

namespace Tiingo.Cryptocurrency
{
    public class ExchangePrice
    {
        private static NumberFormatInfo decimalFormat = new NumberFormatInfo();
        private const int DECIMAL_PRECISION = 25;

        private string? exchangeCode;
        private double? volumeNotional;
        private double? volume;
        private double? high;
        private double? close;
        private double? low;
        private double? open;
        private double? tradesDone;
        private DateTimeOffset? date;

        private static void setDecimalPrecision()
        {
            decimalFormat.NumberDecimalDigits = DECIMAL_PRECISION;
        }

        public ExchangePrice()
        {
            setDecimalPrecision();
        }

        public ExchangePrice(string? exchangeCode,
                             double? volumeNotional,
                             double? volume,
                             double? high,
                             double? close,
                             double? low,
                             double? open,
                             double? tradesDone,
                             DateTimeOffset? date)
        {
            setDecimalPrecision();
            ExchangeCode = exchangeCode;
            VolumeNotional = volumeNotional;
            Volume = volume;
            High = high;
            Close = close;
            Low = low;
            Open = open;
            TradesDone = tradesDone;
            Date = date;
        }

        public ExchangePrice(JToken token)
        {
            setDecimalPrecision();
            ExchangeCode = (string?)token.SelectToken("exchangeCode");
            VolumeNotional = (double?)token.SelectToken("volumeNotional");
            Volume = (double?)token.SelectToken("volume");
            High = (double?)token.SelectToken("high");
            Close = (double?)token.SelectToken("close");
            Low = (double?)token.SelectToken("low");
            Open = (double?)token.SelectToken("open");
            TradesDone = (double?)token.SelectToken("tradesDone");
            Date = ((DateTimeOffset?)token.SelectToken("date"));
            if (Date != null)
                Date = ((DateTimeOffset)Date).ToUniversalTime();
        }

        public string? ExchangeCode { get => exchangeCode; set => exchangeCode = value; }
        public double? VolumeNotional { get => volumeNotional; set => volumeNotional = value; }
        public double? Volume { get => volume; set => volume = value; }
        public double? High { get => high; set => high = value; }
        public double? Close { get => close; set => close = value; }
        public double? Low { get => low; set => low = value; }
        public double? Open { get => open; set => open = value; }
        public double? TradesDone { get => tradesDone; set => tradesDone = value; }
        public DateTimeOffset? Date { get => date; set => date = value; }
    }
}
