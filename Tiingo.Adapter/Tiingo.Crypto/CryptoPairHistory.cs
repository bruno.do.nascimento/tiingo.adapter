﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net;
using Tiingo.Metadata;
using Tiingo.WebCode;

namespace Tiingo.Cryptocurrency
{
    public class CryptoPairHistory
    {
        private WebStatus status;
        private string? ticker;
        private string? baseCurrency;
        private string? quoteCurrency;
        private CryptoPairMetadata? cryptoMetadata;
        private List<ExchangePrice> priceData = new List<ExchangePrice>();
        private SortedDictionary<string, List<ExchangePrice>> exchangeData = new SortedDictionary<string, List<ExchangePrice>>();

        public CryptoPairHistory()
        {
        }

        public CryptoPairHistory(WebStatus status,
                                 CryptoPairMetadata? cryptoMetadata,
                                 List<ExchangePrice> priceData,
                                 SortedDictionary<string, List<ExchangePrice>> exchangeData)
        {
            Status = status;
            CryptoMetadata = cryptoMetadata;
            if (cryptoMetadata != null)
            {
                Ticker = cryptoMetadata.Ticker;
                BaseCurrency = cryptoMetadata.BaseCurrency;
                QuoteCurrency = cryptoMetadata.QuoteCurrency;
            }
            PriceData = priceData;
            ExchangeData = exchangeData;
        }

        public CryptoPairHistory(HttpStatusCode code,
                                 string message,
                                 JObject metadata,
                                 JArray exchanges)
        {
            List<ExchangePrice> exchangePriceList = new List<ExchangePrice>();
            JToken? exchObject = (JToken?)(exchanges != null && exchanges.Count > 0 ? exchanges[0] : (JToken)null);

            Status = new WebStatus(code, (message ?? WebStatus.MSG_HTTPOK));
            if (exchObject != null)
            {
                Ticker = (string?)exchObject.SelectToken("ticker");
                BaseCurrency = (string?)exchObject.SelectToken("baseCurrency");
                QuoteCurrency = (string?)exchObject.SelectToken("quoteCurrency");
                CryptoMetadata = new CryptoPairMetadata(metadata);
                JArray prices = (JArray)exchObject.SelectToken("priceData");
                foreach (JToken price in prices)
                {
                    PriceData.Add(new ExchangePrice(price));
                }
                JToken exchangePrices = exchObject.SelectToken("exchangeData");
                foreach (JToken exchangePrice in exchangePrices)
                {
                    string exchange = exchangePrice.Path.Substring(exchangePrice.Path.LastIndexOf('.') + 1);
                    List<ExchangePrice> exchangeHist = new List<ExchangePrice>();
                    foreach (JToken hist in exchangePrice.Values())
                    {
                        exchangeHist.Add(new ExchangePrice(hist));
                    }
                    ExchangeData.Add(exchange, exchangeHist);
                }
            }
        }

        public WebStatus Status { get => status; set => status = value; }
        public string? Ticker { get => ticker; set => ticker = value; }
        public string? BaseCurrency { get => baseCurrency; set => baseCurrency = value; }
        public string? QuoteCurrency { get => quoteCurrency; set => quoteCurrency = value; }
        public CryptoPairMetadata? CryptoMetadata { get => cryptoMetadata; set => cryptoMetadata = value; }
        public List<ExchangePrice> PriceData { get => priceData; set => priceData = value; }
        public SortedDictionary<string, List<ExchangePrice>> ExchangeData { get => exchangeData; set => exchangeData = value; }
    }
}
