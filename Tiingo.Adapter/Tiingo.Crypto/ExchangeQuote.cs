﻿using Newtonsoft.Json.Linq;
using System;

namespace Tiingo.Cryptocurrency
{
    public class ExchangeQuote
    {
        double? bidPrice;
        double? lastPrice;
        double? askPrice;
        double? bidSize;
        double? lastSize;
        double? askSize;
        double? lastSizeNotional;
        DateTimeOffset? quoteTimestamp;
        string? bidExchange;
        string? askExchange;
        string? lastExchange;
        DateTimeOffset? lastSaleTimestamp;

        public ExchangeQuote()
        {
        }

        public ExchangeQuote(double? bidPrice,
                             double? lastPrice,
                             double? askPrice,
                             double? bidSize,
                             double? lastSize,
                             double? askSize,
                             double? lastSizeNotional,
                             DateTimeOffset? quoteTimestamp,
                             string? bidExchange,
                             string? askExchange,
                             string? lastExchange,
                             DateTimeOffset? lastSaleTimestamp)
        {
            BidPrice = bidPrice;
            LastPrice = lastPrice;
            AskPrice = askPrice;
            BidSize = bidSize;
            LastSize = lastSize;
            AskSize = askSize;
            LastSizeNotional = lastSizeNotional;
            QuoteTimestamp = quoteTimestamp;
            BidExchange = bidExchange;
            AskExchange = askExchange;
            LastExchange = lastExchange;
            LastSaleTimestamp = lastSaleTimestamp;
        }

        public ExchangeQuote(JToken token)
        {
            string? exchangeCode = (string?)token.SelectToken("exchangeCode");

            BidPrice = (double?)token.SelectToken("bidPrice");
            LastPrice = (double?)token.SelectToken("lastPrice");
            AskPrice = (double?)token.SelectToken("askPrice");
            BidSize = (double?)token.SelectToken("bidSize");
            LastSize = (double?)token.SelectToken("lastSize");
            AskSize = (double?)token.SelectToken("askSize");
            LastSizeNotional = (double?)token.SelectToken("lastSizeNotional");
            QuoteTimestamp = (DateTimeOffset?)token.SelectToken("quoteTimestamp");
            if (QuoteTimestamp == null)
                QuoteTimestamp = (DateTimeOffset?)token.SelectToken("quoteTimestampStr");
            if (QuoteTimestamp != null)
                QuoteTimestamp = ((DateTimeOffset)QuoteTimestamp).ToUniversalTime();
            if (exchangeCode != null)
            {
                BidExchange = exchangeCode;
                AskExchange = exchangeCode;
                LastExchange = exchangeCode;
            }
            else
            {
                BidExchange = (string?)token.SelectToken("bidExchange");
                AskExchange = (string?)token.SelectToken("askExchange");
                LastExchange = (string?)token.SelectToken("lastExchange");
            }
            LastSaleTimestamp = (DateTimeOffset?)token.SelectToken("lastSaleTimestamp");
            if (LastSaleTimestamp == null)
                LastSaleTimestamp = (DateTimeOffset?)token.SelectToken("lastSaleTimestampStr");
            if (LastSaleTimestamp != null)
                LastSaleTimestamp = ((DateTimeOffset)LastSaleTimestamp).ToUniversalTime();
        }

        public double? BidPrice { get => bidPrice; set => bidPrice = value; }
        public double? LastPrice { get => lastPrice; set => lastPrice = value; }
        public double? AskPrice { get => askPrice; set => askPrice = value; }
        public double? BidSize { get => bidSize; set => bidSize = value; }
        public double? LastSize { get => lastSize; set => lastSize = value; }
        public double? AskSize { get => askSize; set => askSize = value; }
        public double? LastSizeNotional { get => lastSizeNotional; set => lastSizeNotional = value; }
        public DateTimeOffset? QuoteTimestamp { get => quoteTimestamp; set => quoteTimestamp = value; }
        public string? BidExchange { get => bidExchange; set => bidExchange = value; }
        public string? AskExchange { get => askExchange; set => askExchange = value; }
        public string? LastExchange { get => lastExchange; set => lastExchange = value; }
        public DateTimeOffset? LastSaleTimestamp { get => lastSaleTimestamp; set => lastSaleTimestamp = value; }
    }
}
