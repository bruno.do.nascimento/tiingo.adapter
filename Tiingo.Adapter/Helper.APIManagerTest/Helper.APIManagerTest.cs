using Helper.APIManager;
using NUnit.Framework;
using Tiingo.Services;

namespace Helper.APIManagerTest
{
    public class APIKeyTests
    {
        private static APISelector APIKeySelector = new APISelector(ServiceEndPoints.HOUR_REQUESTS);

        [SetUp]
        public void Setup()
        {

        }

        [Test, Description("")]
        public void TestKeyGenerated()
        {
            string APIKey = APIKeySelector.getAPI();
            Assert.AreEqual(APIKey, "c30f0dd066ea2c3f7d1c2d97e1db87bfbb4c11c7");
        }
    }
}