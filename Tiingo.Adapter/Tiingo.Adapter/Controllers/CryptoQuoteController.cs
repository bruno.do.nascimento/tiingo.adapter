﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Tiingo.Cryptocurrency;
using Tiingo.Services;

namespace Tiingo.Adapter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CryptoQuoteController : ControllerBase
    {
        private static ServiceEndPoints services = new ServiceEndPoints();

        // GET: api/<controller>/AAPL
        [HttpGet("{symbols}")]
        [ResponseType(typeof(List<CryptoPairQuote>))]
        public Task<List<CryptoPairQuote>> Get(string symbols)
        {
            return Task<List<CryptoPairQuote>>.FromResult(Services.GetCryptoQuote(symbols));
        }

        public static ServiceEndPoints Services { get => services; set => services = value; }
    }
}