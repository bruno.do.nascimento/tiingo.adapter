﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Tiingo.Cryptocurrency;
using Tiingo.Services;

namespace Tiingo.Adapter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CryptoIntradayHistoryController : ControllerBase
    {
        private static ServiceEndPoints services = new ServiceEndPoints();

        public static ServiceEndPoints Services { get => services; set => services = value; }

        // GET api/<controller>/GOOG/5
        [HttpGet("{symbolList}/{id:int}")]
        [ResponseType(typeof(List<CryptoPairHistory>))]
        public Task<List<CryptoPairHistory>> Get(string symbols, int id, [FromQuery(Name = "frequency")] string? frequency = null)
        {
            int? numUnits = null;
            string units = null;
            string frequencyPattern = "^(?<units>[0-9]{1,2})(?<time>[A-Z|a-z]{1,4})";

            if (frequency != null)
            {
                Regex processor = new Regex(frequencyPattern);
                Match results = processor.Match(frequency);
                if (results.Success)
                {
                    numUnits = int.Parse(results.Groups["units"].Value);
                    units = results.Groups["time"].Value;
                }
                else
                {
                    numUnits = null;
                    units = frequency;
                }
            }

            return Task<List<CryptoPairHistory>>.FromResult(Services.GetCryptoIntradayHistory(symbols, id, numUnits, units));
        }
    }
}