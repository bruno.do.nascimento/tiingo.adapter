﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Tiingo.Results;
using Tiingo.Services;

namespace Tiingo.Adapter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockQuoteController : ControllerBase
    {
        private static ServiceEndPoints services = new ServiceEndPoints();

        // GET: api/<controller>/AAPL
        [HttpGet("{symbols}")]
        [ResponseType(typeof(List<StockQuote>))]
        public Task<List<StockQuote>> Get(string symbols)
        {
            return Task<List<StockQuote>>.FromResult(Services.GetQuote(symbols));
        }

        public static ServiceEndPoints Services { get => services; set => services = value; }
    }
}