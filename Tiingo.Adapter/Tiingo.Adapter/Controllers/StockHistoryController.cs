﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Tiingo.Results;
using Tiingo.Services;

namespace Tiingo.Adapter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StockHistoryController : ControllerBase
    {
        private static ServiceEndPoints services = new ServiceEndPoints();

        public static ServiceEndPoints Services { get => services; set => services = value; }

        // GET api/<controller>/GOOG/5
        [HttpGet("{symbolList}/{id:int}")]
        [ResponseType(typeof(List<HistoryReturn>))]
        public Task<List<HistoryReturn>> Get(string symbolList, int id)
        {
            List<string> symbols = symbolList.Split(",").ToList();
            return Task<List<HistoryReturn>>.FromResult(Services.GetHistory(symbols, id));
        }
    }
}