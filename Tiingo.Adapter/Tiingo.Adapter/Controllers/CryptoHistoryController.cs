﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Tiingo.Cryptocurrency;
using Tiingo.Services;

namespace Tiingo.Adapter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CryptoHistoryController : ControllerBase
    {
        private static ServiceEndPoints services = new ServiceEndPoints();

        public static ServiceEndPoints Services { get => services; set => services = value; }

        // GET api/<controller>/GOOG/5
        [HttpGet("{symbolList}/{id:int}")]
        [ResponseType(typeof(List<CryptoPairHistory>))]
        public Task<List<CryptoPairHistory>> Get(string symbols, int id)
        {
            return Task<List<CryptoPairHistory>>.FromResult(Services.GetCryptoHistory(symbols, id));
        }

    }
}