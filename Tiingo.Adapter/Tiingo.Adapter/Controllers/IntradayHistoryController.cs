﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.Description;
using Tiingo.Results;
using Tiingo.Services;

namespace Tiingo.Adapter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IntradayHistoryController : ControllerBase
    {
        private static ServiceEndPoints services = new ServiceEndPoints();

        public static ServiceEndPoints Services { get => services; set => services = value; }

        // GET api/<controller>/GOOG/5
        [HttpGet("{symbolList}/{id:int}")]
        [ResponseType(typeof(List<IntradayHistory>))]
        public Task<List<IntradayHistory>> Get(string symbolList, int id, [FromQuery(Name = "frequency")] string? frequency = null)
        {
            int? numUnits = null;
            string units = null;
            string frequencyPattern = "^(?<units>[0-9]{1,2})(?<time>[A-Z|a-z]{1,4})";

            if (frequency != null)
            {
                Regex processor = new Regex(frequencyPattern);
                Match results = processor.Match(frequency);
                if (results.Success)
                {
                    numUnits = int.Parse(results.Groups["units"].Value);
                    units = results.Groups["time"].Value;
                }
                else
                {
                    numUnits = null;
                    units = frequency;
                }
            }

            List<string> symbols = symbolList.Split(",").ToList();
            return Task<List<IntradayHistory>>.FromResult(Services.GetIntradayHistory(symbols, id, numUnits, units));
        }
    }
}